'use strict';

const configGlobal = {};

function configure(options, isGlobal) {
  let config = options;
  if (isGlobal) Object.assign(configGlobal, config);

  if (!config.name) throw new Error('Отсутствует имя приложения');
  return config;
}

module.exports = () => configGlobal;
module.exports.configure = configure;
