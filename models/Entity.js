'use strict';

const co = require('co');

const { SdkClass, SdkData, SdkArrayData } = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, hidden ] = [true, true];

class Entity extends SdkData {
  constructor(data) {
    super();
    this[sym('data')] = {};
    data.options = data.options || {};
    Object.assign(this[sym('data')], data);
  }

  schema() { return SdkClass(this.constructor, this.sdk).schema; }
  toObject() { return super.toObject(this[sym('data')]); }

  static fetch(name, type) {
    let params = { name: name, type };
    let adapterName = this.sdk.config.adapters.config ? 'config' : this.sdk.config.adapter;
    return this.sdk.Adapter.init('Entity', this.schema, this.schemaOptions, adapterName).find(params);
  }

  static get(name) {
    return co(function *() {
      let ent = yield this.fetch(name);
      ent = new this.sdk[ent.Type](ent);
      if (ent.init) yield ent.init();
      return ent;
    }.bind(this));
  }

  static get schema() {
    return {
      name: { type: 'string', required },
      application: { type: 'id', required, referer: 'Application', model: require('./Application')(this.sdk), hidden },
      type: { type: 'string', required },
      options: { type: 'object' },
      rules: { type: 'object' }
    };
  }

  static get schemaOptions() {
    return {
      timestamps: true,
      index: [
        [['name', 'application'], { unique: true }],
      ]
    };
  }

  static findAll(params = {}, options) {
    return co(function *() {
      params = { $and: [params, { application: this.sdk.appId }] };
      let adapterName = this.sdk.config.adapters.config ? 'config' : this.sdk.config.adapter;
      let ents = yield this.sdk.Adapter.init('Entity', this.schema, this.schemaOptions, adapterName)
        .findAll(params, options);
      ents = ents.map(e => (this.name == 'Entity' ? e : new this(Object.assign({}, e))));
      let EntitiesClass = SdkClass(Entities, this.sdk);
      return new EntitiesClass(ents);
    }.bind(this));
  }

  static find(params = {}, options) {
    return co(function *() {
      params = { $and: [params, { application: this.sdk.appId }] };
      let adapterName = this.sdk.config.adapters.config ? 'config' : this.sdk.config.adapter;
      let ent = yield this.sdk.Adapter.init('Entity', this.schema, this.schemaOptions, adapterName)
        .find(params, options);
      ent = (this.name == 'Entity' ? ent : new this(ent));
      if (ent.init) yield ent.init();
      return ent;
    }.bind(this));
  }

  static validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create'].indexOf(access) > -1;

      default: return false;
    }
  }

  validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create', 'update', 'delete'].indexOf(access) > -1;

      default: return false;
    }
  }

  get id() { return this[sym('data')].id; }
  get application() { return this[sym('data')].application; }
  get name() { return this[sym('data')].name; }
  get type() { return this[sym('data')].type; }
  get options() { return this[sym('data')].options; }
  get rules() { return this[sym('data')].rules; }

  set id(value) { return null; }
  set application(value) { return null; }
  set name(value) { if (value) this[sym('data')].name = value; }
  set type(value) { if (value) this[sym('data')].type = value; }
  set options(value) { Object.keys(value || {}).map(k => this[sym('data')].options[k] = value[k]); }
  set rules(value) { if (value) this[sym('data')].options.rules = value; }

}

class Entities extends SdkArrayData {
  schema() { return Entity.schema; }
}

module.exports = SdkClass(Entity);
