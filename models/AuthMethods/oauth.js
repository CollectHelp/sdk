'use strict';

const oauthServer = require('oauth2-server');
const co = require('co');

const BaseAuthMethod = require('./BaseAuthMethod');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, array ] = [true, true];

class OAuth extends BaseAuthMethod {
  constructor(sdk, data, options) {
    super(sdk, data, options);

    this.req = Object.assign({}, sdk.request.req || sdk.request);
    this.req.is = () => true;
    this.req.get = this.req.header = name => this.req.headers[name.toLowerCase()];
    this.req.headers = Object.assign({}, this.req.headers || sdk.request.headers);
    this.req.body = Object.assign(this.req.body || sdk.request.body || {});
    this.req.query = Object.assign(this.req.query || sdk.request.query || {});
    this.data = data || Object.assign({ token: sdk.accessToken }, this.req.query, this.req.body);

    this.server = oauthServer(Object.assign({
      model: this,
      grants: ['password', 'refresh_token'],
    }, this.defaultOptions, this.options));
  }

  static get AccessToken() { return AccessToken; }
  static get RefreshToken() { return RefreshToken; }

  get defaultOptions() {
    return {
      accessTokenLifetime: 60 * 60 * 24,
      refreshTokenLifetime: 60 * 60 * 24 * 7,
    };
  }

  login() {
    return new Promise((resolve, reject) => {
      this.withoutPassword = this.data.password === false;
      this.req.body.username = this.data.username && this.data.username.toString().trim();
      this.req.body.password = this.data.password && this.data.password.toString();
      this.req.headers['content-type'] = 'application/x-www-form-urlencoded';

      let token = (this.req.headers.authorization || '').replace(/basic\s*/i, '');
      let [ clientId, clientSecret ] = Buffer(token, 'base64').toString().split(':');
      this.data.clientId = this.data.clientId || clientId || this.req.body.username;
      this.data.clientSecret = this.data.clientSecret || clientSecret;
      this.data.clientSecret = this.data.clientSecret || (this.sdk.application || {}).secret || Date.now();
      this.req.headers.authorization = 'Basic ' +
        Buffer(this.data.clientId + ':' + this.data.clientSecret).toString('base64');

      this.req.body.grant_type = 'password';
      this.req.method = 'POST';

      this.res = {
        set: () => {},
        jsonp: (token) => {
          token.user = this.req.user;
          this.data.token = token;
          resolve(token);
        }
      };

      this.server.grant()(this.req, this.res, (err) => {
        if (err) { reject(err); }
      });
    });
  }

  auth() {
    return co(function *() {
      let err;
      if (!this.req.headers.authorization && this.data.token) {
        let token = this.data.token.access_token || this.data.token.toString();
        this.req.headers.authorization = 'Bearer ' + token.replace(/bearer\s*/i, '');
      }

      yield new Promise((resolve, reject) => this.server.authorise()(this.req, {}, err => resolve()));

      let { oauth = {} } = this.req;
      let { bearerToken } = oauth;
      if (bearerToken) {
        if (bearerToken.userId) {
          let user = yield this.sdk.User.find({ id: bearerToken.userId }, { populate: 'role' })
            .catch(e => null);
          if (!user) {
            user = yield this.sdk.User(new (this.sdk.constructor))
              .find({ id: bearerToken.userId }, { populate: 'role' })
              .catch(e => null);
          }
          bearerToken.user = user;
        } else {
          let ent = yield this.sdk.Entity.find(bearerToken.clientId);

          if (bearerToken.application.toString() == ent.application.toString()) {
            bearerToken.entity = ent;
          } else {
            throw new this.sdk.Error(404, 'Entity not found');
          }
        }

        return bearerToken;
      }

      return;
    }.bind(this));
  }

  logout() {
    return co(function *() {
      if (this.data.token) {
        yield this.sdk.Adapter.init('AccessToken', AccessToken.schema(this.sdk))
          .remove({ clientId: this.data.token.clientId, userId: this.data.token.userId });
        yield this.sdk.Adapter.init('RefreshToken', RefreshToken.schema(this.sdk))
          .remove({ clientId: this.data.token.clientId, userId: this.data.token.userId });
      } else {
        throw new this.sdk.Error(401, 'Unauthorized');
      }
    }.bind(this));
  }

  refresh() {
    return new Promise((resolve, reject) => {
      if (this.data.token) {
        let token = this.data.token.access_token || this.data.token.toString;
        this.req.headers.authorization = 'Bearer ' + token.replace(/bearer\s*/i, '');
      }

      this.req.headers['content-type'] = 'application/x-www-form-urlencoded';
      this.req.body = this.data || {};
      this.req.body.grant_type = 'refresh_token';

      this.res = {
        set: () => {},
        jsonp: (token) => {
          token.user = this.req.user;
          this.data.token = token;
          resolve(token);
        }
      };

      this.server.grant()(this.req, this.res, (err) => {
        if (err) { reject(err); }
      });
    });
  }

  getAccessToken(accessToken, cb) {
    co(function *() {
      let token = yield this.sdk.Adapter.init('AccessToken', AccessToken.schema(this.sdk)).find({ accessToken });
      if (!token) throw new this.sdk.Error(401, 'Access token not found');
      return token;
    }.bind(this)).then(data => cb(null, data)).catch(err => cb(err));
  }

  saveAccessToken(accessToken, clientId, expires, user, cb) {
    co(function *() {
      let data = {
        accessToken: accessToken,
        application: this.sdk.appId,
        clientId: clientId,
        userId: user.id,
        expires: expires,
      };

      let token = yield this.sdk.Adapter.init('AccessToken', AccessToken.schema(this.sdk)).insert(data);
      if (!token) throw new this.sdk.Error(500, 'Access token not saved');
      return token;
    }.bind(this)).then(data => cb(null, data)).catch(err => cb(err));
  }

  getClient(clientId, clientSecret, cb) {
    if (this.sdk.application && this.sdk.application.secret !== clientSecret) {
      return cb(new this.sdk.Error(401, 'Invalid clientSecret'));
    }

    cb(null, { clientId: clientId, clientSecret: clientSecret });
  }

  grantTypeAllowed(clientId, grantType, cb) {
    if (['password', 'refresh_token'].indexOf(grantType) >= 0) {
      return cb(null, true);
    }

    cb(null, false);
  }

  getUser(username, password, cb) {
    co(function *() {
      let userFilter = { username, password: this.sdk.User.cryptPassword(password) };
      if (this.withoutPassword) delete userFilter.password;
      let user = yield this.sdk.User.find(userFilter, { populate: 'role' });
      if (!user) throw new this.sdk.Error(404, 'User not found');
      return user;
    }.bind(this)).then(data => cb(null, data)).catch(err => cb(err));
  }

  saveRefreshToken(refreshToken, clientId, expires, user, cb) {
    co(function *() {
      var data = {
        refreshToken: refreshToken,
        application: this.sdk.appId,
        clientId: clientId,
        userId: user.id,
        expires: expires,
      };

      let token = yield this.sdk.Adapter.init('RefreshToken', RefreshToken.schema(this.sdk)).insert(data);
      if (!token) throw new this.sdk.Error(500, 'Refresh token not saved');
      return token;
    }.bind(this)).then(data => cb(null, data)).catch(err => cb(err));
  }

  getRefreshToken(refreshToken, cb) {
    co(function *() {
      let token = yield this.sdk.Adapter.init('RefreshToken', RefreshToken.schema(this.sdk)).find({ refreshToken });
      if (!token) throw new this.sdk.Error(401, 'Refresh token not found');
      return token;
    }.bind(this)).then(data => cb(null, data)).catch(err => cb(err));
  }
}

class AccessToken {
  static schema(sdk) {
    return {
      accessToken: { type: 'string' },
      application: { type: 'id', referer: 'Application', model: require('../Application')(sdk) },
      clientId: { type: 'string' },
      userId: { type: 'id', referer: 'User', model: require('../User')(sdk) },
      expires: { type: 'date', expires: 3600 }
    };
  }
}

class RefreshToken {
  static schema(sdk) {
    return {
      refreshToken: { type: 'string' },
      application: { type: 'id', referer: 'Application', model: require('../Application')(sdk) },
      clientId: { type: 'string' },
      userId: { type: 'id', referer: 'User', model: require('../User')(sdk) },
      expires: { type: 'date', expires: 3600 }
    };
  }
}

module.exports = OAuth;
