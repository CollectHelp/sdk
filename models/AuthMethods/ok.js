'use strict';

const md5 = require('md5');
const co = require('co');
const request = require('request');

const BaseAuthMethod = require('./BaseAuthMethod');

class OK extends BaseAuthMethod {
  login() {
    let token = this.data && this.data.token || this.sdk.request.body.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  auth() {
    let req = this.sdk.request;
    let token = (this.data && this.data.token);
    token = token || req.headers.authorization || req.body.token || req.query.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  logout() {
    return Promise.resolve();
  }

  checkToken(token) {
    return co(function *() {
      /*let token = '3cc5a3908031bb1786b20e49aec187efc27b45d49fa4720a9dba5a0b.c7';*/

      let {
        application_key: applicationKey,
        application_secret_key: applicationSecretKey
      } = this.options || {};
      if (!applicationKey || !applicationSecretKey) throw new this.sdk.Error(500, 'Invalid OK config.');

      let str = `${token}${applicationSecretKey}`;
      let secretKey = md5(str);
      let str2 = `application_key=${applicationKey}format=jsonmethod=users.getCurrentUser${secretKey}`;
      let sig = md5(str2);

      let { uid, error_msg: error } = yield this.checkRequest({ token, applicationKey, sig });
      if (!uid) throw new this.sdk.Error(500, error || 'Invalid OK response.');

      return { id: uid };
    }.bind(this));
  }

  checkRequest({ token, applicationKey, sig } = {}) {
    let checkUrl = `https://api.ok.ru/fb.do?` +
      `application_key=${applicationKey}&format=json&method=users.getCurrentUser&sig=${sig}&access_token=${token}`;

    return new Promise((resolve, reject) =>
        request(checkUrl, (err, res, body) => err ? reject(err) : resolve(JSON.parse(body)))
      ).catch(err => Promise.reject(new this.sdk.Error(400, 'Invalid OK token.')));
  }
}

module.exports = OK;
