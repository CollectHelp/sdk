'use strict';

const ldapjs = require('ldapjs');
const co = require('co');

const BaseAuthMethod = require('./BaseAuthMethod');

class LDAP extends BaseAuthMethod {
  constructor(sdk, data, options) {
    super(sdk, data, options);

    this.req = Object.assign({}, sdk.request.req || sdk.request);
    this.req.is = () => true;
    this.req.get = this.req.header = name => this.req.headers[name.toLowerCase()];
    this.req.headers = Object.assign({}, this.req.headers || sdk.request.headers);
    this.req.body = Object.assign(this.req.body || sdk.request.body || {});
    this.req.query = Object.assign(this.req.query || sdk.request.query || {});
    this.data = data || Object.assign({}, this.req.query, this.req.body);
  }

  _connect() {
    return this.connecting || (this.connecting = new Promise((resolve, reject) => {
      let checkConnect = () => (this.ldapClient.connected ? resolve() : setImmediate(checkConnect));
      checkConnect();
      this.ldapClient.on('error', reject);
    }));
  }

  connect() {
    return co(function *() {
      this.ldapClient = this.ldapClient || ldapjs.createClient({
        url: this.generateLdapUrl(this.options)
      });

      try {
        yield this._connect();
      } catch (e) {
        throw new this.sdk.Error(500, 'Error on LDAP server:' + e.message);
      }

      this.connected = true;
    }.bind(this));
  }

  login() {
    var opts = {
      filter: '(uid=' + this.data.username + ')',
      scope: 'sub',
      attributes: []
    };

    return co(function *() {
      yield this.connect();
      yield this.bindP(['', '']);
      let entry = yield this.searchP([this.options.dn, opts]);

      if (this.data.password !== false) {
        yield this.bindP(['cn=' + entry.object.cn + ',' + this.options.dn, this.data.password]);
      }

      return Object.assign({ username: entry.object.uid }, entry);
    }.bind(this));
  }

  auth() {
    return co(function *() {
      var userData = {};
      var token = this.req.headers.authorization;

      if (token) {
        credentials = new Buffer(token.replace(/basic\s*/i, ''), 'base64').toString().split(':');
        this.data.username = credentials[0];
        this.data.password = credentials[1];
      }

      yield this.login();
    }.bind(this));
  }

  logout() {
    return Promise.resolve();
  }

  generateLdapUrl(data) {
    return 'ldap://' + data.host + ':' + data.port + '/dc=' + data.domain;
  }

  bindP(params) {
    return new Promise((resolve, reject) => {
      this.ldapClient.bind(params[0], params[1], (err) => {
        if (err) {
          this.ldapClient.unbind();

          return reject(err);
        }

        resolve();
      });
    });
  }

  searchP(params) {
    return new Promise((resolve, reject) => {
      this.ldapClient.search(params[0], params[1], (err, search) => {
        if (err) { return reject(err); }

        var found = false;

        search.on('end', (result) => {
          if (!found) { reject(new this.sdk.Error(404, 'LDAP user not found')); }
        });

        search.on('searchEntry', (entry) => {
          found = true;

          resolve(entry);
        });
      });
    });
  }
}

module.exports = LDAP;
