'use strict';

class BaseAuthMethod {
  constructor(sdk, data, options) {
    Object.assign(this, { sdk, data, options });
  }

  auth() { throw new this.sdk.Error(500, `Auth method is not implemented for ${this.constructor.name}`); }
  login() { throw new this.sdk.Error(500, `Login method is not implemented for ${this.constructor.name}`); }
  logout() { throw new this.sdk.Error(500, `Logout method is not implemented for ${this.constructor.name}`); }
}

module.exports = BaseAuthMethod;
