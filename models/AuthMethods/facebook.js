'use strict';

const request = require('request');

const BaseAuthMethod = require('./BaseAuthMethod');

class Facebook extends BaseAuthMethod {
  login() {
    let token = this.data && this.data.token || this.sdk.request.body.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  auth({ headers = {}, body = {}, query = {} } = {}) {
    let req = this.sdk.request;
    let token = (this.data && this.data.token);
    token = token || req.headers.authorization || req.body.token || req.query.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  logout() {
    return Promise.resolve();
  }

  checkToken(token) {
    return new Promise((resolve, reject) => {
      /*let token = 'EAAD4NjyqR9EBALCXrDS6bnZB8ZCb4HB2BKTeooW9ELv2GL1oWLZBN0mL2yQpK2VpTveE0v8aY' +
                'pu5VGTjLOZC6e8v1JHjpelUpQLsSZCaLDuzrZC4mXjXlZCpNuVFp5wU0qZBKbCS6q5XQ1g1N1A' +
                '4fQsb3B73vSEAV6JS3b9cyZAwDdFL4SoAJtDPZCy0JsJvrZBZCXv5eU0h4REwmQZDZD';*/

      request({
        url: 'https://graph.facebook.com/me?access_token=' + token,
        method: 'GET'
      }, (err, res, body) => {

        let data = JSON.parse(body);

        if (data.id) {
          resolve(data);
        } else {
          reject(new this.sdk.Error(401, 'Invalid facebook token'));
        }
      });

    });
  }
}

module.exports = Facebook;
