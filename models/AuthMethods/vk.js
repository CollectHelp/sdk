'use strict';

const co = require('co');
const request = require('request');

const BaseAuthMethod = require('./BaseAuthMethod');

class VK extends BaseAuthMethod {
  login() {
    let token = this.data && this.data.token || this.sdk.request.body.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  auth() {
    let req = this.sdk.request;
    let token = (this.data && this.data.token);
    token = token || req.headers.authorization || req.body.token || req.query.token;
    if (!token) {
      return Promise.reject();
    }

    return this.checkToken(token);
  }

  logout() {
    return Promise.resolve();
  }

  checkToken(token) {
    return co(function *() {
      /*let token = '0b5f2f67649017a70453a92927b4003b9dfb84482c3dda6d234d5a31358a1850e401a6ad5def3aee32dd5';*/

      let { client_id: clientId, client_secret: clientSecret } = this.options || {};

      let { access_token: accessToken } = yield this.accessRequest({ clientId, clientSecret });
      let { user_id: id } = yield this.checkRequest({ token, accessToken, clientSecret });

      return { id };
    }.bind(this));
  }

  accessRequest({ clientId, clientSecret } = {}) {
    if (!clientId || !clientSecret) throw new this.sdk.Error(500, 'Invalid VK config.');

    let accessUrl = `https://oauth.vk.com/access_token?v=5.60` +
      `&client_id=${clientId}&client_secret=${clientSecret}&grant_type=client_credentials`;

    return new Promise((resolve, reject) =>
        request(accessUrl, (err, res, body) => err ? reject(err) : resolve(JSON.parse(body) || {}))
      ).catch(err => Promise.reject(new this.sdk.Error(500, 'Invalid VK config.')));
  }

  checkRequest({ token, accessToken, clientSecret } = {}) {
    if (!accessToken) throw new this.sdk.Error(400, 'No access_token.');

    let checkUrl = `https://api.vk.com/method/secure.checkToken?v=5.60` +
      `&access_token=${accessToken}&client_secret=${clientSecret}&token=${token}`;

    return new Promise((resolve, reject) =>
        request(checkUrl, (err, res, body) => {
          if (err) return reject(err);
          body = JSON.parse(body);
          if (!body || body.error) {
            return reject(new Error(body.error.error_msg));
          }
          return resolve(body.response);
        })
      ).catch(err => Promise.reject(new this.sdk.Error(400, 'Invalid VK token.: ' + err.message)));
  }
}

module.exports = VK;
