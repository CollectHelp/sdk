'use strict';

const co = require('co');

const { SdkClass, SdkData, SdkArrayData } = require('./Common');
const Entity = require('./Entity');
const { TableData, TableDatas } = require('./TableData');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, array ] = [true, true];

class Table extends Entity {
  constructor(data = {}) {
    super(data);
  }

  _dropCache() {
    this.sdk.dropEntityCache(this.id);
  }

  static get(name) { return this.fetch(name); }
  static fetch(name) {
    return co(function *() {
      let table = yield this.sdk.Entity.fetch(name, 'Table');
      if (!table) throw new this.sdk.Error(404, 'Table not found');

      this.sdk.Table[name] = new this.sdk.Table(table);
      return this.sdk.Table[name];
    }.bind(this));
  }

  static get schema() {
    let schema = super.schema;
    Object.assign(schema, {
      options: {
        extend: { type: 'string', enum: ['User', 'Role', 'Device'] },
        adapter: { type: 'string', enum: this.sdk.Adapter.list, required, default: 'mongodb' },
        params: {
          index: { type: 'object', array },
          timestamps: { type: 'boolean' },
        },
        fields: { type: 'object' },
        rules: { type: 'object' },
        triggers: { type: 'object', array }
      }
    });

    return schema;
  }

  static swagger(fieldTable = 'table') {
    return co.wrap(function *(swagger, params) {
      if (swagger.application) {
        let fields = params.fields.filter(f => !(f.in == 'path' && f.name == fieldTable));

        let findParams = { type: 'Table', application: swagger.application.id };
        if (swagger.entity) {
          if (swagger.entity.type != 'Table') return false;
          findParams.id = swagger.entity.id;
        }

        let tables = yield this.findAll(findParams);
        tables.map(t => {
          let model = t.toObject();
          model.name = 'TableData_' + model.name;
          model.schema = t.fields;

          let responses = Object.assign({}, params.responses);
          responses[200] = Object.assign({}, responses[200], { model });

          let newPath = Object.assign({}, params, {
            path: params.path.replace(`{${fieldTable}}`, t.name),
            tag: `${params.tag}-${t.name}`,
            description: t.description,
            models: [model].concat(params.models || []),
            responses,
            fields,
          });

          if (newPath.methods[0] == 'post' || newPath.methods[0] == 'put') {
            let name = (`${newPath.tag}-${newPath.path}-${newPath.methods.join('-')}-body`).replace(/\//g, '_');
            newPath.models.push({
              name, schema: model.schema
            });
            newPath.fields.push({
              name: 'body',
              in: 'body',
              type: 'string',
              $ref: `#/definitions/${name}`,
            });
          }

          if (params.paramNames) {
            params.paramNames.map(p => {
              if (p.name == fieldTable) return;
              let i = newPath.fields.findIndex(f => f.in == 'path' && f.name == p.name);
              if (p.optional) {
                let path = newPath.path.replace(`/{${newPath.fields[i].name}}`, '') || '/';
                let fields = [].concat(newPath.fields.slice(0, i), newPath.fields.slice(i + 1));
                swagger.addPath(Object.assign({}, newPath, { path, fields }));
              }
            });
          }

          swagger.addPath(newPath);
        });
      }

      return !swagger.entity;
    }.bind(this));
  }

  validatePermission(name, access) {
    if (!this.options.rules || !this.options.rules[name]) {
      switch (name) {
        case 'all':
          let rule = {
            access: ['list', 'view', 'create', 'update', 'delete'],
            filter: {}
          };

          return this.checkRule(rule, access);

        default: return false;
      }
    }

    return this.checkRule(this.options.rules[name], access);
  }

  checkRule(rule, access) {
    if (rule.access && rule.access.indexOf(access) < 0) return false;
    if (rule.filter)
      Common.parsePlaceholders(
        rule.filter,
        Object.assign({}, this.sdk, this.sdk.request, this.sdk.request.query, this.sdk.request.body)
      );

    return rule;
  }

  set req(value) { this[sym('request')] = value; }
  get req() { return this[sym('request')]; }

  get extend() { return this.options.extend; }
  get extendModel() { return this.options.extend && this.sdk[this.options.extend]; }
  get adapter() { return (this.options.adapter || 'mongodb').toLowerCase(); }
  get fields() { return this.options.fields || {}; }
  get params() { return this.options.params || {}; }
  get index() { return this.params.index; }
  get indexes() { return this.params.index; }
  get timestamps() { return this.params.timestamps; }
  get triggers() { return this.options.triggers; }
  get tablename() { return `TableEntity_${this.id || this.name}`; }

  get db() {
    let { tablename, fields, adapter, index = [], timestamps } = this;
    let options = { index, timestamps };

    if (this.extend && this.sdk[this.extend]) {
      tablename = this.sdk[this.extend].tablename;
      fields = Object.assign({}, fields, this.sdk[this.extend].schema);
      options.timestamps = this.sdk[this.extend].schemaOptions.timestamps;
      options.index = options.index.concat(this.sdk[this.extend].schemaOptions.index);
      adapter = undefined;
    }
    options.cacheName = this.tablename;

    return this[sym('db')] ||
      (this[sym('db')] = this.sdk.Adapter.init(tablename, fields, options, adapter));
  }

  get self() { return this[sym('model')]; }

  runTrigger(trigger, type, prefix, data) {
    return co(function *() {
      if (!trigger.entity) throw new AFError(500, 'No entity for trigger');

      let script = yield this.sdk.Script.get(trigger.entity);
      script.data = { data, type, prefix };
      return yield script.run()
        .then(r => {
          if (r instanceof Error) throw r;
          return r;
        })
        .catch(error => (console.error(error), Object.assign({ error }, data)))
        .then(d => d || data);
    }.bind(this));
  }

  triggerSequence(type, prefix, data) {
    return co(function *() {
      if (!type || !prefix) throw new AFError(500, 'Undefined options on trigger query');

      let triggers = this.triggers || [];

      let errors = [];
      for (let i = 0; i < triggers.length; i++) {
        if (triggers[i].type == type && triggers[i][prefix]) {
          data = yield this.runTrigger(triggers[i], type, prefix, data).catch(e => (errors.push(e), data));
        }
      }

      if (errors.length) data = Object.assign(errors.length == 1 ? { error: errors[0] } : { errors }, data);
      return data;
    }.bind(this));
  }

  findAll(params = {}, options = {}, triggering) {
    return co(function *() {
      options.limit = parseInt(options.limit || this.params.limit);
      let filter = { params, options };
      if (triggering) filter = yield this.triggerSequence('list', 'pre', filter);

      let count = yield this.db.count(filter.params);
      let datas = yield this.db.findAll(filter.params, filter.options);

      datas = new TableDatas(datas, this, Object.assign({ count }, options));
      yield datas.purifyData();

      if (triggering) return yield this.triggerSequence('list', 'post', datas);
      return datas;
    }.bind(this));
  }

  find(params = {}, options = {}, triggering) {
    return co(function *() {
      let filter = { params, options };
      if (triggering) filter = yield this.triggerSequence('view', 'pre', filter);
      Object.assign(filter.options, { limit: 1, offset: 0 });

      let datas = yield this.db.findAll(filter.params, filter.options);
      if (!datas.length) throw new this.sdk.Error(404, 'Table data not found');
      datas = new TableData(datas[0], this, Object.assign({}, filter.options), true);

      if (triggering) return yield this.triggerSequence('view', 'post', datas);
      return datas;
    }.bind(this));
  }

  create(newData) {
    let data = new TableData(newData, this);
    return data.save();
  }

  update(params, newData) {
    return co(function *() {
      let items = yield this.findAll(params);
      yield items.update(newData);
      return items;
    }.bind(this));
  }

  remove(params) {
    return co(function *() {
      yield this.db.remove(params);
      return this.findAll(params);
    }.bind(this));
  }
}

module.exports = SdkClass(Table);
