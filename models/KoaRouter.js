'use strict';

const Router = require('koa-router');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

class KoaRouter extends Router {
  constructor(opts) {
    super(opts);
  }

  register(path, methods, middleware, opts) {
    let paramsRouter;
    if (Array.isArray(middleware) && middleware.length) {
      middleware = middleware.filter(a =>
        (typeof a == 'object' ? (paramsRouter = a) && false : true)
      );
    }

    let route = super.register.call(this, path, methods, middleware, opts);
    let swaggerParams = route.swaggerParams = { methods, fields: [], models: [] };

    if (paramsRouter) {
      paramsRouter.fields = paramsRouter.fields || {};
      let middlewares = Object.keys(paramsRouter).map(p => {
        switch (p) {
          case 'swagger':
            swaggerParams.swaggerFunc = paramsRouter.swagger;
            return;

          case 'tag':
            swaggerParams.tag = Array.isArray(paramsRouter.tag) ? paramsRouter.tag : [paramsRouter.tag];
            return;

          case 'access':
            swaggerParams.auth = true;
            return function *(next) {
              let checkAccess, check;
              let param = paramsRouter[p];
              if (typeof param == 'function')
                checkAccess = this.sdk.Auth.checkAccess.apply(this.sdk.Auth, param(this.sdk));
              if (Array.isArray(param)) checkAccess = this.sdk.Auth.checkAccess.apply(this.sdk.Auth, param);
              if (checkAccess) check = checkAccess(this);
              yield next;
            };

          case 'query':
          case 'body':
          case 'params':
          case 'header':
            break;
          case 'fields':
            let { fields = {}, query = {}, body = {}, params = {}, header = {} } = paramsRouter;

            let parseFields = (obj, defaultIn) => Object.keys(obj).map(f => Object.assign({
              name: f,
              in: defaultIn,
              type: 'string',
            }, obj[f]));

            fields = parseFields(fields, 'body');
            fields = fields.concat(
              parseFields(query, 'query'),
              parseFields(params, 'path'),
              parseFields(header, 'header')
            );
            if (Object.keys(body).length) {
              let model = {
                name: `${paramsRouter.tag}-${path}-${methods.join('-')}-body`.replace(/\//g, '_'),
                schema: body,
              };

              swaggerParams.models.push(model);
              fields.push({
                name: 'body',
                in: 'body',
                type: 'string',
                $ref: `#/definitions/${model.name}`,
              });
            }

            swaggerParams.fields = swaggerParams.fields.concat(fields);
            return;// function *(next) {}; // validation fields

          case 'pagination':
            if (!paramsRouter.pagination) break;
            swaggerParams.pagination = true;
            swaggerParams.fields = swaggerParams.fields.concat([
              { name: 'page', type: 'number', in: 'query' },
              { name: 'offset', type: 'number', in: 'query' },
              { name: 'limit', type: 'number', in: 'query' },
            ]);
            return;// function *(next) {};

          case 'sort':
            if (!paramsRouter.sort) break;
            swaggerParams.sort = true;
            swaggerParams.fields = swaggerParams.fields.concat([
              { name: 'sort', type: 'array', in: 'query', required: false },
            ]);
            return;// function *(next) {};

          case 'responses':
            swaggerParams.responses = paramsRouter.responses;
            return;

          case 'models':
            swaggerParams.models = swaggerParams.models.concat(paramsRouter.models || []);
            return;
        }
      }).filter(m => m);

      if (!paramsRouter.noApp) {
        swaggerParams.fields.push({
          name: 'X-Api-Factory-Application-Id',
          in: 'header',
          type: 'string',
          required: !paramsRouter.maybeApp
        });

        swaggerParams.applicationField = 'X-Api-Factory-Application-Id';

        middlewares.push(function *(next) {
          if (!this.headers['x-api-factory-application-id']) {
            return yield next;
          }

          yield next;
        });
      }

      route.stack = middlewares.concat(route.stack);
    }

    return route;
  }
}

module.exports = KoaRouter;
