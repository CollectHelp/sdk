'use strict';

const Common = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

class Adapter {
  static init(name, schema, options, adapterName) {
    adapterName = adapterName || (this.sdk && this.sdk.config.adapter);
    if (!adapterName) throw new this.sdk.Error(500, 'Empty adapter name');
    adapterName = adapterName.replace(/[\/\\]/, '').toLowerCase();

    let ClAdapter = require(`./Adapters/${adapterName}`);
    let adapters = this.sdk.config.adapters || {};

    return new ClAdapter(name, schema, options, adapters[adapterName], this.sdk);
  }
}

module.exports = Common.SdkClass(Adapter);
