'use strict';

const co = require('co');

const { SdkClass, SdkData, SdkArrayData } = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, system, hidden ] = [true, true, true];

class Device extends SdkData {
  constructor(data = {}) {
    super();
    this[sym('data')] = {};
    Object.assign(this[sym('data')], data);
  }

  schema() { return this.constructor.schema; }
  toRawObject() { return this[sym('data')]; }
  toObject() { return super.toObject(this[sym('data')]); }

  static create(data = {}) {
    let device = new this(data);
    return device.save();
  }

  get tablename() { return `Device_${this.sdk.appId}`; }
  static get tablename() { return `Device_${this.sdk.appId}`; }

  static get schema() {
    return {
      deviceid: { type: 'string', required, system },
      token: { type: 'string', required, system },
      type: { type: 'string', required, system, enum: ['ios', 'android'] },
      user: { type: 'id', required, referer: 'User', model: require('./User')(this.sdk), system },
    };
  }

  static get schemaOptions() {
    return {
      timestamps: true,
      index: [
        [['deviceid', 'user'], { unique: true }],
      ]
    };
  }

  static findAll(params = {}, options = {}) {
    return co(function *() {
      let devices = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions)
        .findAll(params, options);
      devices = devices.map(d => new this(Object.assign({}, d)));
      let DevicesClass = SdkClass(Devices)(this.sdk);
      return new DevicesClass(devices);
    }.bind(this));
  }

  static find(params = {}, options = {}) {
    return co(function *() {
      let device = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions).find(params, options);
      if (!device) throw new this.sdk.Error(404, 'Device not found');
      return new this(device);
    }.bind(this));
  }

  static validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'create'].indexOf(access) > -1;

      case 'own':
        if (['list', 'view', 'delete'].indexOf(access) > -1)
          return { filter: { user: this.sdk.user.id } };
        return false;

      default: return false;
    }
  }

  validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create', 'update', 'delete'].indexOf(access) > -1;

      case 'own':
        if (this.id == this.sdk.user.id)
          if (['list', 'view', 'update', 'delete'].indexOf(access) > -1)
            return { filter: { user: this.sdk.user.id } };
        return false;

      default: return false;
    }
  }

  get id() { return this[sym('data')].id ? this[sym('data')].id.toString() : null; }
  get deviceid() { return this[sym('data')].deviceid; }
  get token() { return this[sym('data')].token; }
  get type() { return this[sym('data')].type; }
  get user() { return this[sym('data')].user; }

  set id(value) {};
  set deviceid(value) { this[sym('data')].deviceid = value; }
  set token(value) { this[sym('data')].token = value; }
  set type(value) { this[sym('data')].type = value; }
  set user(value) { this[sym('data')].user = value; }

  save() {
    return co(function *() {
      let device;
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;

      if (this.id) {
        device = yield this.sdk.Adapter.init(this.tablename, schema, options).update(this[sym('data')]);
      } else {
        device = yield this.sdk.Adapter.init(this.tablename, schema, options).insert(this[sym('data')]);
      }

      Object.keys(device).map(k => this[sym('data')][k] = device[k]);

      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      Object.keys(newData).map(f => this[f] = newData[f]);
      return this.save();
    }.bind(this));
  }

  remove() {
    return co(function *() {
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;
      yield this.sdk.Adapter.init(this.tablename, schema, options).remove({ id: this.id });
      Object.keys(this[sym('data')]).map(k => delete this[sym('data')][k]);
      return this;
    }.bind(this));
  }
}

class Devices extends SdkArrayData {
  schema() { return Device.schema; }

  save() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].save();
      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].update(newData);
      return this;
    }.bind(this));
  }

  remove() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].remove(newData);
      this.map((d, i) => delete this[i]);
      this.length = 0;
      return this;
    }.bind(this));
  }
}

module.exports = SdkClass(Device);
