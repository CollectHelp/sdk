'use strict';

const co = require('co');
const vm = require('vm');
const request = require('request');
const fs = require('fs');
const ch = require('child_process');

const { SdkClass, PromiseWrap } = require('./Common');
const Entity = require('./Entity');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, array ] = [true, true, true];

class Script extends Entity {
  constructor(data = {}) {
    super(data);
  }

  init() {
    return co(function *() {
      let code = yield PromiseWrap(cb => fs.readFile(`${this.scriptsPath}/${this.name}.js`, cb))
        .catch(e => undefined);
      this.options.code = code && code.toString();
    }.bind(this));
  }

  static get scriptsPath() { return `${this.sdk.config.rootDir}/scripts/`; }
  get scriptsPath() { return `${this.sdk.config.rootDir}/scripts/`; }

  static get(name) { return this.fetch(name); }
  static fetch(name) {
    return co(function *() {
      let script = yield this.sdk.Entity.fetch(name, 'Script');

      yield PromiseWrap(cb => fs.access(`${this.scriptsPath}/${name}.js`, fs.constants.R_OK, cb))
        .catch(e => { if (!script) throw new this.sdk.Error(404, 'Script not found', e); });

      if (!script) script = { name, type: 'Script', application: this.sdk.appId };

      this.sdk.Script[name] = new this.sdk.Script(script);
      yield this.sdk.Script[name].init();
      return this.sdk.Script[name];
    }.bind(this));
  }

  static get schema() {
    let schema = this.sdk.Entity.schema;
    Object.assign(schema, {
      options: {
        code: { type: 'string' },
        rules: { type: 'object' }
      }
    });

    return schema;
  }

  static swagger(fieldTable) {
    return co.wrap(function *(swagger, params) {
    }.bind(this));
  }

  set data(data = {}) { this.sdk.request.body = data; }

  validatePermission(name, access) {
    if (!this.options.rules || !this.options.rules[name]) {
      switch (name) {
        case 'all':
          var rule = {
            access: ['run']
          };

          return this.checkRule(rule, access);

        default: return false;
      }
    }

    return this.checkRule(this.options.rules[name], access);
  }

  checkRule(rule, access) {
    if (rule.access && rule.access.indexOf(access) < 0) return false;
    return rule;
  }

  get context() {
    return {
      co: co,
      console: console,
      Promise: Promise,
      request: request,
      require: require,
      fs: fs,
      ch: ch,
      Buffer: Buffer,
      Date: Date,
      Math: Math,
      __dirname: __dirname,
      name: this.name,

      data: Object.assign({}, this.sdk.request.query || {}, this.sdk.request.body || {}),
      req: Object.assign({}, this.sdk, this.sdk.request),

      SDK: this.sdk
    };
  }

  run() {
    return new Promise((resolve, reject) => {
      let resolved = false;
      let done = data => {
        if (!resolved) {
          resolved = true;
          resolve(data);
        }
      };

      let context = Object.assign({ done }, this.sdk, this.context);

      let err;
      try {
        if (this.options.async) {
          err = vm.runInNewContext(
`let func = async function() {
  ${this.options.code}
}.bind(this);
func().then(done).catch(done);\n`,
            context
          );
        } else {
          err = vm.runInNewContext(
`co(function *(){
  ${this.options.code}
}.bind(this)).then(done).catch(done);\n`,
            context
          );
        }
      } catch (e) {
        done(e);
      }
    });
  }
}

module.exports = SdkClass(Script);
