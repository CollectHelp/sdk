'use strict';

const co = require('co');

const BaseAdapter = require('./BaseAdapter');

class ConfigDB extends BaseAdapter {
  get wait() {
    this._wait = this._wait || Promise.resolve();
    return this._wait;
  }

  set wait(value) {
    this._wait = this.wait.then(() => value);
  }

  init(options, sdk) {
    this.sdk = sdk;
    this.config = sdk.config;
    this.populates = {};

    Object.keys(this.schema).map(f => {
      if (['id', 'referer'].indexOf(this.schema[f].type) >= 0 && this.schema[f].populate) {
        this.populates[f] = this.schema[f].populate;
      }
    });

    return this;
  }

  getPopulates(populates, options = {}) {
    return co(function *() {
      for (let f in populates) {
        if (!this.schema[f]) continue;
        if (this.schema[f].model) this.schema[f].model = yield Promise.resolve(this.schema[f].model);
        if (!this.schema[f].model || !Object.keys(populates[f]).length) continue;

        let opt = (typeof options[f] == 'object' && options[f]) || {};
        let filter = {};
        if (opt.filter) {
          filter = opt.filter;
          delete opt.filter;
        }
        filter = { $and: [filter, { id: { $in: Object.keys(populates[f]) } }] };

        let data = yield this.schema[f].model.findAll(filter, opt);
        if (data && data.length) data.map(d => d.id ? populates[f][d.id] = d.toObject() : null);
      }
      return populates;
    }.bind(this));
  }

  static toId(id) {
    if (id && !Array.isArray(id) && id.toString && id.toString().match(/^[0-9a-fA-F]{24}$/))
      return id;
  }
  toId(id) { return this.constructor.toId(id); }

  toType(value, type) {
    return co(function *() {
      if (typeof value == 'object' && !this.toId(value) && !(value instanceof Date))
        return this.improveFilter(value, type);

      switch (type) {
        case 'id':
        case 'referer': return this.toId(value);

        case 'number':
        case 'integer':
        case 'float': return parseFloat(value);

        case 'string': return value + '';

        case 'boolean': return !value || value == 'false' ? false : true;

        case 'date': return value instanceof Date ? value.getTime() : parseFloat(value);

        case 'geo':
        case 'object':
        case 'file':
        default: return value;
      }
    }.bind(this));
  }

  improveFilter(object, type) {
    return co(function *() {
      if (typeof object == 'string' || this.toId(object)) {
        object = yield this.toType(object, type || 'id');
        return type ? object : { _id: object };
      }

      if (Array.isArray(object)) {
        let array = object.concat([]);
        for (let i in array)
          array[i] = yield this.improveFilter(array[i], type);
        return array;
      }

      if (object instanceof Date) return this.toType(object, type);

      if (object === null) return null;

      if (typeof object == 'object') {
        let deepRefs = {};
        object = Object.assign({}, object);
        for (let i in object) {
          let deep = i.split('.');
          switch (i) {
            case 'id':
              object._id = object.id;
              delete object.id;
              i = '_id';
              type = 'id';
              break;
            case '$exists': type = 'boolean'; break;
          }

          if (deep.length > 1 && this.schema[deep[0]]) {
            if (['id', 'referer'].indexOf(this.schema[deep[0]].type) >= 0 && this.schema[deep[0]].model) {
              deepRefs[deep[0]] = deepRefs[deep[0]] || {};
              deepRefs[deep[0]][deep.slice(1).join('.')] = object[i];
              delete object[i];
              continue;
            }
          }

          if (!type && this.schema[i]) type = this.schema[i].type;
          object[i] = yield this.improveFilter(object[i], type);
        }

        if (Object.keys(deepRefs).length) {
          for (let i in deepRefs) {
            let model = yield Promise.resolve(this.schema[i].model);
            let data = yield model.findAll(deepRefs[i], { select: [] });
            object[i] = yield this.toType({ $in: data.map(d => d.id) }, 'id');
          }
        }
      }

      return object;
    }.bind(this));
  }

  _filter(filter, data, fieldName) {
    let filterType = typeof filter;
    if (filter instanceof Date) filterType = 'date';
    if (Array.isArray(filter)) filterType = 'array';

    switch (filterType) {
      case 'date':
      case 'string':
      case 'boolean':
        if (Array.isArray(data[fieldName]))
          return data[fieldName].indexOf(filter) >= 0;
        else
          return data[fieldName] == filter;

      case 'array':
        if (Array.isArray(data[fieldName]))
          return data[fieldName].filter(d => filter.indexOf(d) >= 0).length > 0;
        else
          return filter.indexOf(data[fieldName]) >= 0;

      case 'object':
        if (!Object.keys(filter).length) return true;

        let results = Object.keys(filter).map(k => {
          if (!fieldName && this.schema[k]) return this._filter(filter[k], data, k);
          switch (k) {
            case '$and': return filter[k].map(f => this._filter(f, data, fieldName)).indexOf(false) < 0;
            case '$or': return filter[k].map(f => this._filter(f, data, fieldName)).indexOf(true) >= 0;
            case '$exists': return (data[fieldName] !== undefined && filter[k]) || !filter[k];
            case '$in': return this._filter(filter[k], data, fieldName);

            default: return this._filter(filter[k], data, fieldName || k);
          }
        });

        return results.indexOf(false) < 0;

      default: return data[fieldName] == filter;
    }
  }

  _find(filter, params) {
    let tableName = this.tableName.toLowerCase();
    let data = this.config[tableName] || this.config[tableName + 's'];
    if (!data) return [];
    data = Array.isArray(data) ? data : [data];
console.log(filter);
    data = data
      .map(d => Object.assign({}, d))
      .filter(d => this._filter(filter, d));

    return data;
  }

  findAll(filter = {}, params = {}) {
    let populates = {};
    let populateOptions = {};
    params = Object.assign({}, params);

    params.limit = parseInt(params.limit || this.sdk.config.defaultLimit);
    if (params.limit || params.page || params.page === 0 || params.offset || params.offset === 0) {
      params.limit = params.limit || 10;
      params.skip = parseInt(params.offset) || parseInt(params.page) * params.limit;
    }

    let { populate = [] } = params;
    if (typeof populate == 'string') {
      populate = populate.split(',');
    }
    let p1 = Array.isArray(populate) ? populate : Object.keys(populate);
    let p2 = Array.isArray(this.populates) ? this.populates : Object.keys(this.populates);
    p1.concat(Object.keys(this.populates)).map(p => populates[p] = {});
    populateOptions = Object.assign({}, this.populates, Array.isArray(populate) ? {} : populate);

    delete params.populate;
    if (params.select) params.select = params.select.join(' ');

    return co(function *() {
      if (filter.id) { this.idStack.push(filter.id.toString()); }
      filter = yield this.improveFilter(filter);

      if (typeof filter != 'object') throw new this.sdk.Error(404, 'Not found');

      let docs = this._find(filter, params);
      yield this.wait;

      for (let i = 0; i < docs.length; i++) {
        docs[i].id = docs[i]._id;
        delete docs[i]._id;

        Object.keys(populates).map(f => {
          if (!docs[i][f]) return;
          if (this.schema[f].array) {
            docs[i][f].map(id => id ? populates[f][id] = null : null);
            return;
          }
          populates[f][docs[i][f]] = null;
          return;
        });

        if (this.timestamps) {
          docs[i].createdAt = parseInt(docs[i].createdAt.getTime());
          docs[i].updatedAt = parseInt(docs[i].updatedAt.getTime());
        }
      }

      if (Object.keys(populates).length) {
        populates = yield this.getPopulates(populates, populateOptions);

        docs.map(d => Object.keys(populates).map(f => {
          if (!d[f]) return;
          if (this.schema[f].array) {
            d[f] = d[f].map(id => Object.assign({}, populates[f][id]));
            return;
          }
          d[f] = Object.assign(populates[f][d[f]]);
        }));
      }

      this.idStack = [];
      return docs;
    }.bind(this));
  }

  find(filter, params = {}) {
    return co(function *() {
      Object.assign(params, { limit: 1, skip: 0 });
      let docs = yield this.findAll(filter, params);
      return docs[0];
    }.bind(this));
  }

  save(data, notNew) {
    return co(function *() {
      return yield this.find(data.id);
    }.bind(this));
  }

  flushModel() {}
}

module.exports = ConfigDB;
