'use strict';

class BaseAdapter {
  constructor(tableName, schema, options = {}, config, sdk) {
    Object.assign(this, { tableName, schema, options, cacheName: (options.cacheName || tableName) });
    if (this.init) { this.init(config, sdk); }
  }

  count() { throw new Error('Unsupport method'); }
  findAll() { throw new Error('Unsupport method'); }
  find() { throw new Error('Unsupport method'); }
  insert() { throw new Error('Unsupport method'); }
  update() { throw new Error('Unsupport method'); }
  remove() { throw new Error('Unsupport method'); }
}

module.exports = BaseAdapter;
