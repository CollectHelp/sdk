'use strict';

const co = require('co');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const BaseAdapter = require('./BaseAdapter');

const connections = {};

class MongoDB extends BaseAdapter {
  static get connections() { return connections; }

  get wait() {
    this._wait = this._wait || Promise.resolve();
    return this._wait;
  }

  set wait(value) {
    this._wait = this.wait.then(() => value);
  }

  init({ host = 'localhost', port = 27017, name } = {}, sdk) {
    this.sdk = sdk;
    if (!name && this.sdk) name = (this.sdk.config.name || '').replace(/[^a-z0-9]/ig, '-').toLowerCase();
    if (!name) throw this.sdk.Error(500, 'Empty DB name');

    let connStr = `mongodb://${host}:${port}/${name}`;
    if (!connections[connStr]) connections[connStr] = mongoose.createConnection(connStr);
    this.conn = connections[connStr];
    this.conn.cache = this.conn.cache || {};

    this.idStack = [];
    if (this.conn.cache[this.cacheName]) {
      this.conn.models[this.tableName] = this.conn.cache[this.cacheName];

      this.Schema = this.conn.models[this.tableName].schema;
      this.schema = this.Schema.schema;

      this.Model = this.conn.models[this.tableName];
      this.populates = this.Model.populates;
    } else {
      delete this.conn.models[this.tableName];
      this.populates = {};

      this.Schema = this.convertScheme(this.schema, this.options);
      this.Schema.schema = this.schema;

      this.Model = this.conn.model(this.tableName, this.Schema);
      this.Model.populates = this.populates;

      this.conn.cache[this.cacheName] = this.conn.models[this.tableName];
    }
    return this;
  }

  convertScheme(scheme, options) {
    let sc = {};
    let opts = Object.assign({ versionKey: false }, options);

    Object.keys(scheme).map(k => sc[k] = this.getSchemeField(k, scheme[k]));

    let S = new mongoose.Schema(sc, opts);

    Object.keys(opts).map(k => {
      switch (k) {
        case 'index':
          opts[k].map(index => {
            if (Array.isArray(index[0])) {
              let indexFields = {};
              index[0].map(f => indexFields[f] = f[0] == '-' ? -1 : 1);
              index[0] = indexFields;
            }
            S.index(index[0], index[1]);
          });
          break;
      }
    });

    return S;
  }

  getSchemeField(name, field) {
    let sc = {};

    switch (field.type) {
      case 'number':
      case 'integer':
      case 'float': sc.type = mongoose.Schema.Types.Number; break;
      case 'string': sc.type = mongoose.Schema.Types.String; break;
      case 'boolean': sc.type = mongoose.Schema.Types.Boolean; break;
      case 'date': sc.type = mongoose.Schema.Types.Date; break;
      case 'geo':
        sc = new mongoose.Schema({
          coordinates: {
            type: [mongoose.Schema.Types.Number],
            index: '2dsphere',
          },
        }, { versionKey: false });
        break;

      case 'id':
      case 'referer':
        if ((field.referer || field.ref) && !field.model) {
          let ref = field.referer || field.ref;
          if (typeof ref == 'string' || this.toId(ref)) ref = this.sdk.Table.fetch(ref);
          field.model = ref;
        }
        if (field.populate) this.populates[name] = field.populate;

        sc.type = mongoose.Schema.Types.ObjectId;
        break;

      case 'object':
	if (field.schema) {
          sc = this.convertScheme(field.schema, { _id: false });
          return field.array ? [sc] : sc;
        }
        sc.type = mongoose.Schema.Types.Object;
        sc._id = false;
        break;

      case 'file':
        sc.type = mongoose.Schema.Types.Mixed;
        sc._id = false;
        break;

      default:
        if (field && typeof field == 'object') return this.convertScheme(field, { _id: false });
        sc.type = mongoose.Schema.Types.Mixed;
        sc._id = false;
    }

    ['required', 'trim', 'uppercase', 'lowercase'].map(p => sc[p] = !!field[p]);
    ['default', 'enum', 'min', 'max', 'expires'].map(p => sc[p] = field[p]);

    if (field.array) sc = [sc];

    return sc;
  }

  getPopulates(populates, options = {}) {
    return co(function *() {
      for (let f in populates) {
        if (!this.schema[f]) continue;
        if (this.schema[f].model) this.schema[f].model = yield Promise.resolve(this.schema[f].model);
        if (!this.schema[f].model || !Object.keys(populates[f]).length) continue;

        let opt = (typeof options[f] == 'object' && options[f]) || {};
        let filter = {};
        if (opt.filter) {
          filter = opt.filter;
          delete opt.filter;
        }
        filter = { $and: [filter, { id: { $in: Object.keys(populates[f]) } }] };

        let data = yield this.schema[f].model.findAll(filter, opt);
        if (data && data.length) data.map(d => d.id ? populates[f][d.id] = d.toObject() : null);
      }
      return populates;
    }.bind(this));
  }

  static toId(id) {
    if (id && !Array.isArray(id) && id.toString && id.toString().match(/^[0-9a-fA-F]{24}$/))
      return mongoose.Types.ObjectId(id);
  }
  toId(id) { return this.constructor.toId(id); }

  toType(value, type) {
    return co(function *() {
      if (typeof value == 'object' && !this.toId(value) && !(value instanceof Date))
        return this.improveFilter(value, type);

      switch (type) {
        case 'id':
        case 'referer': return this.toId(value);

        case 'number':
        case 'integer':
        case 'float': return parseFloat(value);

        case 'string': return value + '';

        case 'boolean': return !value || value == 'false' ? false : true;

        case 'date': return value instanceof Date ? value.getTime() : parseFloat(value);

        case 'geo':
        case 'object':
        case 'file':
        default: return value;
      }
    }.bind(this));
  }

  improveFilter(object, type) {
    return co(function *() {
      if (typeof object == 'string' || this.toId(object)) {
        object = yield this.toType(object, type || 'id');
        return type ? object : { _id: object };
      }

      if (Array.isArray(object)) {
        let array = object.concat([]);
        for (let i in array)
          array[i] = yield this.improveFilter(array[i], type);
        return array;
      }

      if (object instanceof Date) return this.toType(object, type);

      if (object === null) return null;

      if (typeof object == 'object') {
        let deepRefs = {};
        object = Object.assign({}, object);
        for (let i in object) {
          let deep = i.split('.');
          switch (i) {
            case 'id':
              object._id = object.id;
              delete object.id;
              i = '_id';
              type = 'id';
              break;
            case '$exists': type = 'boolean'; break;
          }

          if (deep.length > 1 && this.schema[deep[0]]) {
            if (['id', 'referer'].indexOf(this.schema[deep[0]].type) >= 0 && this.schema[deep[0]].model) {
              deepRefs[deep[0]] = deepRefs[deep[0]] || {};
              deepRefs[deep[0]][deep.slice(1).join('.')] = object[i];
              delete object[i];
              continue;
            }
          }

          if (!type && this.schema[i]) type = this.schema[i].type;
          object[i] = yield this.improveFilter(object[i], type);
        }

        if (Object.keys(deepRefs).length) {
          for (let i in deepRefs) {
            let model = yield Promise.resolve(this.schema[i].model);
            let data = yield model.findAll(deepRefs[i], { select: [] });
            object[i] = yield this.toType({ $in: data.map(d => d.id) }, 'id');
          }
        }
      }

      return object;
    }.bind(this));
  }

  count(filter) {
    return co(function *() {
      if (this.toId(filter)) { filter = { id: this.toId(filter) }; }
      filter = yield this.improveFilter(filter);
      return this.Model.find(filter).count().exec();
    }.bind(this));
  }

  /**
   * @param  {object} filter
   * @param  {object} params
   * @param  {string|array} params.join
   * @param  {number} params.limit
   * @param  {number} params.page
   * @param  {number} params.offset
   */
  findAll(filter = {}, params = {}) {
    let populates = {};
    let populateOptions = {};
    params = Object.assign({}, params);

    params.limit = parseInt(params.limit || this.sdk.config.defaultLimit);
    if (params.limit || params.page || params.page === 0 || params.offset || params.offset === 0) {
      params.limit = params.limit || 10;
      params.skip = parseInt(params.offset) || parseInt(params.page) * params.limit;
    }

    let { populate = [] } = params;
    if (typeof populate == 'string') {
      populate = populate.split(',');
    }
    let p1 = Array.isArray(populate) ? populate : Object.keys(populate);
    let p2 = Array.isArray(this.populates) ? this.populates : Object.keys(this.populates);
    p1.concat(Object.keys(this.populates)).map(p => populates[p] = {});
    populateOptions = Object.assign({}, this.populates, Array.isArray(populate) ? {} : populate);

    delete params.populate;
    if (params.select) params.select = params.select.join(' ');

    return co(function *() {
      if (filter.id) { this.idStack.push(filter.id.toString()); }
      filter = yield this.improveFilter(filter);

      if (typeof filter != 'object') throw new this.sdk.Error(404, 'Not found');

      let find = this.Model.find(filter, null, params);
      let docs = yield find.exec();
      yield this.wait;

      for (let i = 0; i < docs.length; i++) {
        docs[i] = docs[i].toObject();
        docs[i].id = docs[i]._id;
        delete docs[i]._id;

        Object.keys(populates).map(f => {
          if (!docs[i][f]) return;
          if (this.schema[f].array) {
            docs[i][f].map(id => id ? populates[f][id] = null : null);
            return;
          }
          populates[f][docs[i][f]] = null;
          return;
        });

        if (this.timestamps) {
          docs[i].createdAt = parseInt(docs[i].createdAt.getTime());
          docs[i].updatedAt = parseInt(docs[i].updatedAt.getTime());
        }
      }

      if (Object.keys(populates).length) {
        populates = yield this.getPopulates(populates, populateOptions);

        docs.map(d => Object.keys(populates).map(f => {
          if (!d[f]) return;
          if (this.schema[f].array) {
            d[f].map((id, k) => d[f][k] = populates[f][id]);
            return;
          }
          d[f] = populates[f][d[f]];
        }));
      }

      this.idStack = [];
      return docs;
    }.bind(this));
  }

  find(filter, params = {}) {
    return co(function *() {
      Object.assign(params, { limit: 1, skip: 0 });
      let docs = yield this.findAll(filter, params);
      return docs[0];
    }.bind(this));
  }

  save(data, notNew) {
    if (this.options.timestamps) {
      delete data.updatedAt;
    }

    return co(function *() {
      data = yield this.improveFilter(data);
      let model = new this.Model(data);
      model.isNew = !notNew;

      let err = model.validateSync();
      if (err) {
        let message = err.message;
        if (err.errors) {
          message = Object.keys(err.errors).map(k => err.errors[k].message);
          message = message.join(' \n');
        }
        throw new this.sdk.Error(400, message, err);
      }

      yield this.wait;
      try {
        var res = yield model.save();
      } catch (err) {
        if (err.code == 11000) {
          throw new this.sdk.Error(400, 'Already exists');
        }
        throw new this.sdk.Error(500, err.message);
      }

      return yield this.find(model._id);
    }.bind(this));
  }

  insert(data) {
    return this.save(data);
  }
  update(data) {
    return this.save(data, true);
  }

  remove(params) {
    return co(function *() {
      if (this.toId(params)) { params = { id: this.toId(params) }; }
      params = yield this.improveFilter(params);

      return this.Model.remove(params);
    }.bind(this));
  }

  flushModel() {
    if (this.conn.cache[this.cacheName]) {
      delete this.conn.cache[this.cacheName];
    }
    if (this.conn.models[this.tableName]) {
      delete this.conn.models[this.tableName];
    }
  }
}

module.exports = MongoDB;
