'use strict';

const co = require('co');
const fs = require('fs');
const ch = require('child_process');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

const Common = {
  parsePlaceholders: (object, placeholders) => {
    if (typeof object == 'string') {
      let placeholdersMatch = object.match(/{\$([\d\w\-+_.]+)}/ig);

      if (!placeholdersMatch) return object;

      if (placeholdersMatch.length == 1 && object.length == placeholdersMatch[0].length)
        return Common.getMatchValue(placeholdersMatch[0].replace(/[${}]/g, ''), placeholders);

      return object.replace(/{\$([\d\w\-+_.]+)}/ig,
        (ph, name) => Common.getMatchValue(name, placeholders) ? Common.getMatchValue(name, placeholders) : '');
    }

    if (Array.isArray(object)) {
      return object.map(o => Common.parsePlaceholders(o, placeholders));
    }

    if (typeof object == 'object') {
      let newObject = {};

      for (let i in object) {
        let n = Common.parsePlaceholders(i, placeholders);
        newObject[n] = Common.parsePlaceholders(object[i], placeholders);
      }

      return newObject;
    }

  },

  getMatchValue: (name, object) => {
    let path = Array.isArray(name) ? name : name.split('.');
    name = path.shift();

    if (path.length && object) return Common.getMatchValue(path, object[name]);
    return object ? object[name] : undefined;
  },

  applyEntity: (options, req, application) => {
    application = application || req.application;
    if (application.constructor.name == 'Application') application = application.id;

    return require('./Entity.js').get(options.entity, application)
      .then(ent => {
        switch (ent.type) {
          case 'Service': return ent.setData(req.body).request(options.route, req);
          case 'Script': return ent.setData(req.body, req).run();
        }
      });
  },

  saveFile: (value, path) => co(function *() {
    let file = {};
    if (!value.buffer && !value.path) throw new Error('Empty file buffer');

    let name = value.name || value.originalname;
    if (!name) throw new Error('Empty file name');

    let buff = value.buffer;

    file.mimetype = value.type;
    file.originalname = value.originalname;
    file.size = value.size;

    yield Common.PromiseWrap(cb => ch.exec(`mkdir -p ${path}`, cb));

    if (buff) {
      yield Common.PromiseWrap(cb => fs.writeFile(`${path}/${name}`, buff, cb));
    } else {
      yield Common.PromiseWrap(cb => ch.exec(`mv ${value.path} ${path}/${name}`, cb));
    }

    return file;
  }.bind(this)),

  PromiseWrap: func => new Promise((resolve, reject) => func((err, data) => err ? reject(err) : resolve(data))),

  SdkClass: (cl, sdk) => {
    function SdkClass() {
      if (!(this instanceof cl)) return Common.SdkClass.bind(null, cl).apply(null, arguments);
      if (!sdk) throw new Error('SDK is not initialized');
      let C = cl.bind(cl);
      Array.from(arguments).map(a => C = cl.bind(cl, a));
      let result = new C();
      Object.assign(this, result, { get sdk() { return sdk; } });
    };

    if (cl.__proto__.name == 'SdkClass') cl.__proto__ = cl.__proto__.__proto__;
    SdkClass.__proto__ = cl;
    SdkClass.prototype = cl.prototype;
    if (sdk) Object.assign(SdkClass, { get sdk() { return sdk; } });
    return SdkClass;
  },

  get SdkData() { return SdkData; },
  get SdkArrayData() { return SdkArrayData; }
};

class SdkData extends Object {
  constructor(data = {}) {
    super(data);
    Object.assign(this, data);
  }

  schema() { return undefined; }
  data() { return this.toObject(); }
  toObject(data) {
    let obj = Object.assign({}, data || this);
    if (this.schema()) Object.keys(this.schema()).map(f =>
      this.schema()[f].hidden && (delete obj[f])
    );
    return obj;
  }

  valueOf() { return this.toObject(); }
  toJSON() { return { fields: this.schema(), data: this.toObject() }; }
  toString() {
    let val = this.valueOf();
    return typeof val == 'object' ? JSON.stringify(val) : val.toString();
  }
}

class SdkArrayData {
  constructor(data = [], options = {}) {
    Object.assign(this, data);
    this.length = data.length;
    ['count', 'limit', 'page', 'offset', 'sort'].map(k => this[sym(k)] = options[k]);
  }

  schema() { return undefined; }
  data() { return this.toObject(); }
  toObject() { return this.map(d => d.toObject() ? d.toObject() : d); }

  count() { return this[sym('count')] ? parseInt(this[sym('count')]) : this.length; }
  limit() { return this[sym('limit')] ? parseInt(this[sym('limit')]) : this[sym('limit')]; }
  page() { return this[sym('page')] ? parseInt(this[sym('page')]) : this[sym('page')]; }
  offset() { return this[sym('offset')] ? parseInt(this[sym('offset')]) : this[sym('offset')]; }
  sort() { return this[sym('sort')]; }

  get map() { return Array.from(this).map; }

  valueOf() { return this.toObject(); }
  toString() { return JSON.stringify(Object.assign(this.toJSON(), { fields: undefined })); }
  toJSON() {
    let json = { fields: this.schema(), data: this.toObject() };
    ['count', 'limit', 'page', 'offset', 'sort'].map(k => json[k] = this[k]());
    return json;
  }
}

module.exports = Common;
