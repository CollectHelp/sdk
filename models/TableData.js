'use strict';

const co = require('co');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

const { SdkData, SdkArrayData } = require('./Common');
const File = require('./fields/File.js');

class TableData extends SdkData {
  constructor(data = {}, table, options, notNew = false) {
    if (table.extendModel) Object.assign(data, new table.extendModel(data).toObject());

    super(data);

    this[sym('options')] = options;
    this[sym('table')] = table;
    this[sym('options')] = options;
    this[sym('isNew')] = !(this.id || this.id === 0) && !notNew;
  }

  get sdk() { return this[sym('table')].sdk; }

  schema() {
    if (this[sym('table')].extendModel)
      return Object.assign({}, this[sym('table')].fields, this[sym('table')].extendModel.schema);
    return this[sym('table')].fields;
  }
  options() { return this[sym('options')]; }

  runTrigger(trigger, type, prefix, newData) {
    return co(function *() {
      if (!trigger.entity) throw new AFError(500, 'No entity for trigger');

      let script = yield this.sdk.Script.get(trigger.entity);
      script.data = { data: this, type, prefix, newData };
      let res = yield script.run()
        .then(r => {
          if (r instanceof Error) throw r;
          return r;
        })
        .catch(e => { console.error(e); throw new this.sdk.Error(500, 'Error in trigger', e); });
      return this;
    }.bind(this));
  }

  triggerSequence(type, prefix, data) {
    return co(function *() {
      if (!type || !prefix) throw new AFError(500, 'Undefined options on trigger query');

      let triggers = this[sym('table')].triggers || [];

      for (let i = 0; i < triggers.length; i++) {
        if (triggers[i].type == type && triggers[i][prefix]) {
          yield this.runTrigger(triggers[i], type, prefix, data);
        }
      }

      return this;
    }.bind(this));
  }

  validateData() {
    return co(function *() {
      for (let i in this) {
        let v = v => v;
        let f = f => true;

        let field = this.schema()[i];
        if (!field) {
          let ts = this[sym('table')].timestamps;
          if (!ts && this[sym('table')].extendModel) ts = this[sym('table')].extendModel.schemaOptions.timestamps;
          if (ts && typeof ts == 'object' && [ts.createdAt, ts.updatedAt].indexOf(i) >= 0) continue;
          if (ts && typeof ts != 'object' && ['createdAt', 'updatedAt'].indexOf(i) >= 0) continue;

          this[i] = i == 'id' ? this[i] : undefined;
          continue;
        }

        let isArray = field.array;
        this[i] = (isArray && !Array.isArray(this[i])) ? [this[i]] : this[i];

        switch (field.type) {
          case 'string':
            v = v => (v != null ? v + '' : '');
            f = f => (f !== null);
            break;

          case 'number':
            v = v => parseFloat(v);
            f = f => (f !== null && !isNaN(parseFloat(f)));
            break;

          case 'boolean':
            v = v => !!v;
            f = f => (f !== null);
            break;

          case 'file':
            this[i] = Array.isArray(this[i]) ? this[i] : [this[i]];
            f = f => (f && Object.keys(f).length);

            for (let k in this[i]) {
              if (this[i][k]) {
                let data = typeof this[i][k] == 'string' ? this[i][k] : this[i][k][0] || this[i][k];
                this[i][k] = yield File.validate(field, data);
              }
            }

            if (!isArray) this[i] = this[i][0];
            break;

          case 'referer':
            f = f => (f && (this[sym('table')].db.toId(f) || this[sym('table')].db.toId(f.id)));
            v = v => (!this[sym('table')].db.toId(v) && v.id ? v.id : v);
            break;

          case 'geo':
            f = f => (f && (typeof f == 'object' || typeof f == 'array'));
            v = v => ({ coordinates: [parseFloat(v.lng || v[0]) || 0, parseFloat(v.lat || v[1]) || 0] });
            break;

          case 'date':
            let sec = !!field.sec;
            f = f => (f && (f instanceof Date || !isNaN(parseInt(f)) || f.toString().toUpperCase() == 'NOW'));
            v = v => {
              if (v.toString().toUpperCase() == 'NOW') {
                v = Date.now();
                if (sec) v = v / 1000;
              }
              return new Date(parseInt(v));
            };
            break;
        }

        if (isArray) {
          this[i] = this[i].filter(f).map(v).filter(f);
        } else {
          this[i] = f(this[i]) ? v(this[i]) : undefined;
          this[i] = f(this[i]) ? this[i] : undefined;
        }
      }

      return this;
    }.bind(this));
  }

  purifyData() {
    return co(function *() {
      for (let i in this) {
        let v = v => v;

        let field = this.schema()[i];
        if (!field) {
          let ts = this[sym('table')].timestamps;
          if (!ts && this[sym('table')].extendModel) ts = this[sym('table')].extendModel.schemaOptions.timestamps;
          if (ts && typeof ts == 'object' && [ts.createdAt, ts.updatedAt].indexOf(i) >= 0) continue;
          if (ts && typeof ts != 'object' && ['createdAt', 'updatedAt'].indexOf(i) >= 0) continue;
          this[i] = i == 'id' ? this[i] : undefined;
          continue;
        }

        let isArray = field.array;
        this[i] = (isArray && !this[i]) ? [] : this[i];
        this[i] = (isArray && !Array.isArray(this[i])) ? [this[i]] : this[i];

        let filteredOptions = this.filterOptions(i);

        switch (field.type) {
          case 'file':
            this[i] = Array.isArray(this[i]) ? this[i] : [this[i]];
            for (let k in this[i])
              this[i][k] = yield File.get(
                field,
                this[i][k],
                filteredOptions
              );
            if (!isArray) this[i] = this[i][0];
            break;

          case 'date':
            v = v => parseInt(v.getTime());
            break;

          case 'geo':
            v = v => ([v.coordinates[0], v.coordinates[1]]);
            break;

        }

        if (isArray) {
          this[i] = this[i].map(v);
        } else {
          this[i] = v(this[i]);
        }
      }

      return this;
    }.bind(this));
  }

  filterOptions(fieldKey) {
    let options = Object.assign({}, this.options());

    for (let opt in options)
      if (options[opt] && options[opt][fieldKey]) options[opt] = options[opt][fieldKey];

    return options;
  }

  save() {
    return co(function *() {
      if (this[sym('isNew')]) yield this.triggerSequence('create', 'pre', this);
      yield this.triggerSequence('save', 'pre');
      yield this.validateData();

      let data;
      if (this[sym('isNew')]) {
        data = yield this[sym('table')].db.insert(this);
      } else {
        data = yield this[sym('table')].db.update(this);
      }

      Object.keys(this).map(f => (delete this[f]));
      Object.assign(this, data);

      yield this.purifyData();
      yield this.triggerSequence('save', 'post');
      console.log(this[sym('isNew')]);
      if (this[sym('isNew')]) yield this.triggerSequence('create', 'post');

      this[sym('isNew')] = false;

      return this;
    }.bind(this));
  }

  update(data) {
    return co(function *() {
      yield this.triggerSequence('update', 'pre', data);

      data = Object.assign({}, data);
      Object.assign(this, data);

      yield this.save();
      yield this.triggerSequence('update', 'post', data);

      return this;
    }.bind(this));
  }

  remove() {
    return co(function *() {
      yield this.triggerSequence('delete', 'pre');
      yield this[sym('table')].db.remove({ id: this.id });
      yield this.triggerSequence('delete', 'post');
      Object.keys(this).map(f => (delete this[f]));
      return this;
    }.bind(this));
  }
}

class TableDatas extends SdkArrayData {
  constructor(data = [], table, options = {}) {
    data = data.map((d, i) => new TableData(Object.assign({}, d), table, Object.assign({}, options)));
    super(data);

    this[sym('table')] = table;
    return this;
  }

  schema() {
    if (this[sym('table')].extendModel)
      return Object.assign({}, this[sym('table')].fields, this[sym('table')].extendModel.schema);
    return this[sym('table')].fields;
  }

  purifyData() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].purifyData();
      return this;
    }.bind(this));
  }

  save() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].save();
      return this;
    }.bind(this));
  }
  update(data) {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].update(data);
      return this;
    }.bind(this));
  }
  remove() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].remove();
      this.map((d, i) => delete this[i]);
      this[sym('length')] = 0;
      return this;
    }.bind(this));
  }
}

module.exports = { TableData, TableDatas };
