'use strict';

const md5 = require('md5');
const co = require('co');

const { SdkClass, SdkData, SdkArrayData } = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, system, hidden ] = [true, true, true];

class User extends SdkData {
  constructor(data = {}) {
    super();
    this[sym('data')] = {};
    if (!data.id && data.password) data.password = User.cryptPassword(data.password);
    Object.assign(this[sym('data')], data);
  }

  schema() { return this.constructor.schema; }
  toRawObject() { return this[sym('data')]; }
  toObject() { return super.toObject(this[sym('data')]); }

  static create(data = {}) {
    let user = new this(data);
    return user.save();
  }

  get tablename() { return this.sdk.appId ? `User_${this.sdk.appId}` : 'User'; }
  static get tablename() { return this.sdk.appId ? `User_${this.sdk.appId}` : 'User'; }

  static get schema() {
    return {
      username: { type: 'string', required, system },
      password: { type: 'string', required, system, hidden },
      role: { type: 'id', default: null, referer: 'Role', model: require('./Role')(this.sdk), system },
      type: { type: 'string', default: 'oauth', system },
    };
  }

  static get schemaOptions() {
    return {
      timestamps: true,
      index: [
        [['username', 'type'], { unique: true }],
      ]
    };
  }

  static findAll(params = {}, options = {}) {
    return co(function *() {
      let users = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions).findAll(params, options);
      users = users.map(u => new this(Object.assign({}, u)));
      let UsersClass = SdkClass(Users)(this.sdk);
      return new UsersClass(users);
    }.bind(this));
  }

  static find(params = {}, options = {}) {
    return co(function *() {
      let user = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions).find(params, options);
      if (!user) throw new this.sdk.Error(404, 'User not found');
      return new this(user);
    }.bind(this));
  }

  static cryptPassword(password) {
    return md5(password);
  }

  static validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'create'].indexOf(access) > -1;

      case 'own':
        if (['list', 'view', 'update', 'delete'].indexOf(access) > -1)
          return { filter: { id: this.sdk.user.id } };
        return false;

      default: return false;
    }
  }

  login() {
    return this.sdk.Auth.login({ username: this.username, password: false });
  }

  validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create', 'update', 'delete'].indexOf(access) > -1;

      case 'own':
        if (this.id == this.sdk.user.id)
          if (['list', 'view', 'update', 'delete'].indexOf(access) > -1)
            return {
              filter: { id: this.sdk.user.id },
              select: ['username', 'role', 'type', 'createdAt', 'updatedAt']
            };
        return false;

      default: return false;
    }
  }

  get id() { return this[sym('data')].id ? this[sym('data')].id.toString() : null; }
  get username() { return this[sym('data')].username; }
  get password() { return this[sym('data')].password; }
  get type() { return this[sym('data')].type; }
  get role() { return this[sym('data')].role; }
  get application() { return this[sym('application')]; }

  set username(value) { this[sym('data')].username = value; }
  set password(value) {
    if (this[sym('data')].password != value) {
      this[sym('data')].password = this.constructor.cryptPassword(value);
    }
  }
  set type(value) { this[sym('data')].type = value; }
  set role(value) { this[sym('data')].role = value; }
  set id(value) {};

  save() {
    return co(function *() {
      let user;
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;
      if (this[sym('data')].role === '') this[sym('data')].role = undefined;

      if (this.id) {
        user = yield this.sdk.Adapter.init(this.tablename, schema, options).update(this[sym('data')]);
      } else {
        user = yield this.sdk.Adapter.init(this.tablename, schema, options).insert(this[sym('data')]);
      }

      Object.keys(user).map(k => this[sym('data')][k] = user[k]);

      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      Object.keys(newData).map(f => this[f] = newData[f]);
      return this.save();
    }.bind(this));
  }

  remove() {
    return co(function *() {
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;
      yield this.sdk.Adapter.init(this.tablename, schema, options).remove({ id: this.id });
      Object.keys(this[sym('data')]).map(k => delete this[sym('data')][k]);
      return this;
    }.bind(this));
  }
}

class Users extends SdkArrayData {
  schema() { return User.schema; }

  save() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].save();
      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].update(newData);
      return this;
    }.bind(this));
  }

  remove() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].remove(newData);
      this.map((d, i) => delete this[i]);
      this.length = 0;
      return this;
    }.bind(this));
  }
}

module.exports = SdkClass(User);
