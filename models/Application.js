'use strict';

const co = require('co');

const SDK = require('../index');
const { SdkClass, SdkData } = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const [ required, array, populate ] = [true, true, true];

class Application extends SdkData {
  constructor(data = {}) {
    super();
    this[sym('data')] = {};
    data.options = data.options || {};
    Object.assign(this[sym('data')], data);
  }

  schema() { return SdkClass(this.constructor, this.sdk).schema; }
  toObject() { return super.toObject(this[sym('data')]); }

  _dropCache() {
    this.sdk.dropApplicationCache(this.id);
  }

  static find(params = {}, options) {
    return co(function *() {
      let adapterName = this.sdk.config.adapters.config ? 'config' : this.sdk.config.adapter;
      let app = yield this.sdk.Adapter.init('Application', this.schema, this.schemaOptions, adapterName)
        .find(params, options);
      if (!app) throw new this.sdk.Error(404, 'Application not found');
      return new this(app);
    }.bind(this));
  }

  static get schema() {
    return {
      name: { type: 'string', required },
      secret: { type: 'string', required },
      creator: {
        type: 'id', populate: { select: ['username'] },
        referer: 'User', model: require('./User')(new SDK()) },
      admins: {
        type: 'id', required, array, populate: { select: ['username'] },
        referer: 'User', model: require('./User')(new SDK()),
      },
      options: { type: 'object' },
    };
  }

  static get schemaOptions() {
    return {
      timestamps: true,
      index: [
        [['name', 'creator'], { unique: true }],
      ]
    };
  }

  static validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create'].indexOf(access) > -1;

      case 'own':
        if (['list', 'view', 'update', 'delete'].indexOf(access) > -1) {
          return {
            filter: {
              $or: [
                { creator: req.user.id },
                { admins: req.user.id }
              ]
            }
          };
        }
        return false;

      default: return false;
    }
  }

  validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create', 'update', 'delete'].indexOf(access) > -1;

      case 'own':
        if (this.admins && this.admins.map(a => (a.id || a).toString()).indexOf(this.sdk.user.id) > -1)
          if (['list', 'view', 'update', 'delete'].indexOf(access) > -1)
            return { filter: { admins: req.user.id } };
        if (this.creator && this.creator.id == this.sdk.user.id)
          if (['list', 'view', 'update', 'delete'].indexOf(access) > -1)
            return { filter: { creator: req.user.id } };

        return false;

      default: return false;
    }
  }

  get id() { return this[sym('data')].id ? this[sym('data')].id.toString() : null; }
  get name() { return this[sym('data')].name; }
  get secret() { return this[sym('data')].secret; }
  get creator() { return this[sym('data')].creator; }
  get admins() { return this[sym('data')].admins || []; }
  get options() { return this[sym('data')].options; }
}

module.exports = SdkClass(Application);
