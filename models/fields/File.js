'use strict';

const co = require('co');
const mongoose = require('mongoose');
const fs = require('fs');
const ch = require('child_process');
//const lwip = require('node-lwip');
const path = require('path');
const request = require('request');

const { PromiseWrap, saveFile } = require('../Common');

const FileField = {
  validate: (field = {}, value) => co(function *() {
    if (!value) return null;
    if (value.id || (value.originalname && value.path)) return value;

    if (!field.source) field.source = 'local';

    if (typeof value == 'string' && value.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)) {
      value = { url: value };

    }

    if (value.url && typeof value.url === 'string')
      value = yield FileField.downloadFile(value.url);

    switch (field.source) {
      case 'local':
        if (typeof value != 'object') return null;
        return FileField.saveFile(value);

      default:
        return null;
    }
  }.bind(this)),

  get: (field = {}, value, options = {}) => co(function *() {
    if (!value) return value;

    if (!field.source) field.source = 'local';

    let actionList = ['crop', 'resize', 'scale', 'rotate'];
    let optionsArr = Object.keys(options)
      .filter(key =>
        actionList.indexOf(key) >= 0 && options[key] && Object.keys(options[key]).length > 0
      )
      .map(action => Object.assign({ action }, options[action]))
      .sort((a, b) => (!b.priority || a.priority < b.priority ? -1 : 1));

    if (optionsArr.length > 0) return FileField.modifyData(value, optionsArr);

    switch (field.source) {
      case 'local':
        return value;

      default:
        return null;
    }
  }.bind(this)),

  downloadFile: url => co(function *() {
    let filename = FileField.getFileName(url);
    try {
      return yield new Promise((resolve, reject) =>
        request({ url: url, encoding: null }, function(err, res, body) {
          if (err) reject(err);

          const statusCode = res.statusCode;
          const contentType = res.headers['content-type'];
          let error;

          if (statusCode !== 200) error = new Error(`Request Failed. Status Code: ${statusCode}`);

          if (!contentType) error = new Error(`No content-type`);

          if (error) reject(error);

          resolve({ type: contentType, name: filename, size: body.length, buffer: body });
        })
      );
    }
    catch (err) {throw new Error(err.message);}
  }.bind(this)),

  saveFile: (value) => co(function *() {
    if (!value.size || !value.name) {
      throw new Error('Empty file buffer');
    }

    value.originalname = value.name;
    value.id = mongoose.Types.ObjectId.createPk();
    value.name = value.id + '_' + value.name.replace(/[\s\(\)]/g, '_');

    let file = yield saveFile(value, `${__dirname}/../../../../files`);
    file.path = '/files/' + value.name;

    return file;
  }.bind(this)),

  modifyData: (fileData, params)=> co(function *() {
    let modifiedData = yield FileField.getDataFromCash(fileData.path, fileData.originalname, params);
    if (!modifiedData) modifiedData = yield FileField.getModifiedData(fileData.path, fileData.originalname, params);

    return {
      path: modifiedData.path,
      mimetype: fileData.mimetype,
      originalname: modifiedData.originalname,
      size:  modifiedData.size
    };
  }.bind(this)),

  getRootPath: () => path.join(__dirname, '../../../..'),
  getFileType: filePath => path.extname(filePath).substr(1),
  isImage: filePath => ['png', 'jpeg', 'jpg', 'gif'].indexOf(FileField.getFileType(filePath)) >= 0,
  getFileName: filePath => filePath.slice(filePath.lastIndexOf(path.sep) + 1, filePath.length),

  generateModifiedFilePath: (origPath, params) => {
    let pos = origPath.lastIndexOf('/') + 1;
    let newDir = origPath.slice(0, pos);
    let newName = origPath.slice(pos, origPath.lastIndexOf('.'));

    for (let index = 0; index < params.length; index++) {
      let elements = params[index];
      let dir = elements.action;
      let actionParams = [];
      for (let key in elements)
        if (key !== 'action' && key !== 'priority') actionParams.push(elements[key]);
      newDir += `${dir}/`;
      newName += `&${actionParams.join('_')}`;
    }

    return {
      newDir: `${newDir}`,
      newName: `${newName}.${FileField.getFileType(origPath)}`
    };
  },

  crop: (image, params)=> co(function *() {
    let {
      left = 0,
      top = 0,
      right = image.width(),
      bottom = image.height()
    } = params;

    return yield PromiseWrap(cb => image.crop(+left, +top, +right - 1, +bottom - 1, cb));
  }.bind(this)),

  resize: (image, params) => co(function *() {
    let {
      width = image.width(),
      height = image.height()
    } = params;

    return yield PromiseWrap(cb => image.resize(+width, +height, cb));
  }.bind(this)),

  scale: (image, params) => co(function *() {
    let {
      ratio = 0
     } = params;

    return yield PromiseWrap(cb => image.scale(+ratio, cb));
  }.bind(this)),

  rotate: (image, params) => co(function *() {
    let {
      angle = 0
    } = params;

    return yield PromiseWrap(cb => image.rotate(+angle, cb));
  }.bind(this)),

  modifyImage: (image, action, params) => co(function *() {
    switch (action){
      case 'crop':
        return FileField.crop(image, params);
      case 'resize':
        return FileField.resize(image, params);
      case 'scale':
        return FileField.scale(image, params);
      case 'rotate':
        return FileField.rotate(image, params);
    }
  }.bind(this)),

  getModifiedData: (unmodifiedFilePath, originalname, params)=> co(function *() {
    let newPath = FileField.generateModifiedFilePath(unmodifiedFilePath, params);
    let dir = path.join(FileField.getRootPath(), newPath.newDir);

    let fullPath = path.join(FileField.getRootPath(), newPath.newDir + newPath.newName);
    let filePath = path.join(FileField.getRootPath(), unmodifiedFilePath);
    let fileType = FileField.getFileType(unmodifiedFilePath);

    let modifiedImage = yield PromiseWrap(cb => lwip.open(filePath, fileType, cb))
      .catch(e => { throw new Error('Error modifying file'); });

    for (let index = 0; index < params.length; index++) {
      let config = {};
      let param = params[index];

      for (let key in param)
        if (key !== 'action' && key !== 'priority') config[key] = param[key];

      modifiedImage = yield FileField.modifyImage(modifiedImage, param.action, config);
    }

    let modifiedFile = yield PromiseWrap(cb => modifiedImage.toBuffer(fileType, cb));

    if (modifiedFile) {
      yield PromiseWrap(cb => ch.exec(`mkdir -p ${dir}`, cb));
      yield PromiseWrap(cb => fs.writeFile(fullPath, modifiedFile, cb));
    }

    if (!modifiedFile) throw new Error('Error modifying file');

    return {
      path: newPath.newDir + newPath.newName,
      originalname,
      size: modifiedFile.length
    };
  }.bind(this)),

  getDataFromCash: (origPath, originalname, params)=> co(function *() {
    let filePath = FileField.generateModifiedFilePath(origPath, params);
    let fullPath = path.join(FileField.getRootPath(), filePath.newDir + filePath.newName);
    let fileStats = yield PromiseWrap(cb => fs.stat(fullPath, cb)).catch(e => null);

    if (!fileStats) return;

    return {
      path: filePath.newDir + filePath.newName,
      originalname,
      size: fileStats.size
    };
  }.bind(this)),
};

module.exports = FileField;
