'use strict';

const Router = require('express').Router;
const Common = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

class ExpressRouter extends Router {}

module.exports = Common.SdkClass(ExpressRouter);
