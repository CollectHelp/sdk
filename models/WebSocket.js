'use strict';

const Common = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

class WebSocket {}

module.exports = Common.SdkClass(WebSocket);
