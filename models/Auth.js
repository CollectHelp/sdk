'use strict';

const co = require('co');

const Common = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));

class Auth {
  static get types() { return { OAuth, LDAP }; }

  static register(data, method = 'basic') {
    return co(function *() {
      let methods = this.sdk.config.register || {};

      if (method == 'basic' || (methods[method] && methods[method].type == 'basic')) {
        return this.registerMethods.basic(data, methods[method]);
      }

      if (methods[method]) {
        let ent = yield this.sdk.Entity.get(methods[method].entity);
        return this.registerMethods[ent.type](ent, data, methods[method]);
      }

      throw new this.sdk.Error(400, 'Register method not found');
    }.bind(this));
  }

  static get registerMethods() {
    return {
      basic: (data, options = {}) => {
        let body = data || this.sdk.request.body || {};
        if (!body.username || !body.password) {
          throw new this.sdk.Error(400, 'Username and password are required for basic register');
        }

        if (this.sdk.config.register.close) {
          throw new this.sdk.Error(403, 'Register is forbidden');
        }

        let role = options.role || this.sdk.config.register.defaultRole;

        let user = new this.sdk.User({
          username: body.username,
          password: body.password,
          role: role
        });

        return user.save();
      },

      Service: (service, data, options) => {
        return co(function *() {
          service.data = data || this.sdk.request.body;
          let res = yield service.request(options.route);
          if (res.statusCode != 200) throw new this.sdk.Error(res.statusCode, res.body);
          return res.body;
        }.bind(this));
      },

      Script: (script, data, options) => {
        return co(function *() {
          script.data = data || this.sdk.request.body;
          let res = yield script.run();
          if (res instanceof Error) throw new this.sdk.Error(res);
          return res;
        }.bind(this));
      },
    };
  }

  static authorize(data) {
    return co(function *() {
      if (!this.sdk) throw new this.sdk.Error(500, 'SDK is not initialized');

      this.sdk.isAuthorised = false;
      let options = this.sdk.config.auth || {};

      if (options.oauth) {
        this.sdk.accessToken = yield this.integratedAuth('oauth', data, options.oauth);
      } else {
        let errors = [];
        let keysAuth = Object.keys(options);
        for (let i = 0; i < keysAuth.length; i++) {
          if (!options[keysAuth[i]]) continue;

          if (options[keysAuth[i]].entity) {
            this.sdk.accessToken = yield this.customAuth(keysAuth[i], data, options[keysAuth[i]])
              .catch(e => (errors.push(e), null));
          } else {
            this.sdk.accessToken = yield this.integratedAuth(keysAuth[i], data, options[keysAuth[i]])
              .catch(e => (errors.push(e), null));
          }

          if (this.sdk.accessToken) break;
        }
      }

      this.sdk.user = this.sdk.accessToken ? this.sdk.accessToken.user : null;
      this.sdk.role = this.sdk.user ? this.sdk.user.role : null;
      this.sdk.isAuthorised = !!this.sdk.user;

      if (this.sdk.role && !this.sdk.role.permissions) {
        throw new this.sdk.Error(403, 'Permission denied');
      }

      return this.sdk.accessToken;
    }.bind(this));
  }

  static integratedAuth(name, data, options) {
    return co(function *() {
      let IntegratedAuth = require(`./AuthMethods/${name.replace(/[\/\\]/, '').toLowerCase()}`);
      return (new IntegratedAuth(this.sdk, data, options)).auth();
    }.bind(this));
  }

  static customAuth(name, data, options) {
    return co(function *() {
      let script = yield this.sdk.Script.get(options.entity);
      script.data = data || Object.assign({}, this.sdk.request.query, this.sdk.request.body);
      return script.run();
    }.bind(this));
  }

  static checkAccess(entity, access) {
    return req => {
      req.getFilter = req.getFilter || function(query) {
        let filter = {};
        this.filters = this.filters || [];
        filter = { $and: this.filters.concat([query]) };
        if (this.publicFilter) filter = { $or: [this.publicFilter, filter] };
        if (this.importantFilters && this.importantFilters.length) {
          this.importantFilters.push(filter);
          filter = { $and: this.importantFilters };
        }
        return filter;
      };

      req.getFilterSelect = req.getFilterSelect || function(select) {
        if (!this.select) return select;
        if (!select) return this.select;
        return select.filter(s => this.select.indexOf(s) >= 0);
      };

      let entObj;
      let entName;

      let models = {
        application: this.sdk.Application,
        user: this.sdk.User,
        role: this.sdk.Role,
        device: this.sdk.Device,
        entity: this.sdk.Entity,
      };

      entObj = req[entity] || models[entity];
      if (['application', 'device', 'user', 'role', 'entity'].indexOf(entity) >= 0) entName = 'sys_' + entity;
      else entName = entObj.name;

      if (!entObj) {
        throw new this.sdk.Error(403, 'Permission denied');
      }

      let publicAccess = entObj.validatePermission('public', access, Object.assign({}, req));
      if (publicAccess) {
        req.publicFilter = typeof publicAccess == 'object' ? publicAccess.filter : undefined;
        req.select = typeof publicAccess == 'object' ? publicAccess.select : undefined;
      }

      if (!this.sdk.isAuthorised) {
        if (publicAccess) return;
        throw new this.sdk.Error(401, 'Unauthorized');
      }

      if (!this.sdk.role) {
        return;
      }

      let userPerms = this.sdk.role.permissions || {};

      if (!userPerms[entName] && !userPerms.all) {
        if (publicAccess) return;
        throw new this.sdk.Error(403, 'Permission denied');
      }

      let check = false;
      let filters = [];
      let importantFilters = [];
      let selects = [];

      for (let i = 0; i < 2; i++) {
        let p = [entName, 'all'][i];

        if (userPerms[p]) {
          for (let k in userPerms[p]) {
            let userPerm = userPerms[p][k];

            let v = entObj.validatePermission(k, access);

            if (!v) { continue; }

            if (userPerm.indexOf(access) > -1) {
              if (v.filter) v.important ? importantFilters.push(v.filter) : filters.push(v.filter);
              if (v.select) selects = selects.concat(v.select);
              check = true;
            }
          }
        }
      }

      if (filters.length) {
        req.filters = req.filters || [];
        req.filters.push({ $or: filters });
      }

      if (importantFilters.length) {
        req.importantFilters = req.importantFilters || [];
        req.importantFilters = req.importantFilters.concat(importantFilters);
      }

      if (selects.length) {
        req.select = (req.select || []).concat(selects);
        req.select = req.select.filter((s, p) => req.select.indexOf(s) == p);
      }

      if (!check) {
        if (publicAccess) return;
        throw new this.sdk.Error(403, 'Permission denied');
      }

      return;
    };
  }

  static login(data, method) {
    return co(function *() {
      let options = this.sdk.config.auth || {};

      if ((method && !Array.isArray(method)) && options[method]) {
        if (options[method].entity)
          this.sdk.accessToken = yield this.customLogin(method, data, options[method]);
        else
          this.sdk.accessToken = yield this.integratedLogin(method, data, options[method]);
      } else {

        let errors = [];
        let keysLogin = method || Object.keys(options);

        if (options.oauth) {
          if (~keysLogin.indexOf('oauth')) keysLogin.splice(keysLogin.indexOf('oauth'), 1);
          keysLogin.unshift('oauth');
        }

        for (let i = 0; i < keysLogin.length; i++) {
          let opts = options[keysLogin[i]];
          if (!opts) continue;

          if (opts.entity)
            this.sdk.accessToken = yield this.customLogin(keysLogin[i], data, opts)
              .catch(e => (errors.push(e), null));
          else
            this.sdk.accessToken = yield this.integratedLogin(keysLogin[i], data, opts)
              .catch(e => (errors.push(e), null));

          if (this.sdk.accessToken) break;
        }

        if (!this.sdk.accessToken) {
          throw new this.sdk.Error(400, errors);
        }
      }

      this.sdk.user = this.sdk.accessToken ? this.sdk.accessToken.user : null;
      this.sdk.role = this.sdk.user ? this.sdk.user.role : null;
      this.sdk.isAuthorised = !!this.sdk.user;

      if (this.sdk.role && !this.sdk.role.permissions) {
        throw new this.sdk.Error(403, 'Permission denied');
      }

      return this.sdk.accessToken;
    }.bind(this));
  }

  static integratedLogin(name, data, options = {}) {
    return co(function *() {
      name = (options.type || name).replace(/[\/\\]/, '').toLowerCase();
      let IntegratedLogin = require(`./AuthMethods/${name}`);
      let il = (new IntegratedLogin(this.sdk, data, options));
      let token = yield il.login();

      if (name != 'oauth' && this.sdk.config.auth.oauth) token = yield this.checkLogin(token, name, data, options);
      return token;
    }.bind(this));
  }

  static customLogin(name, data, options) {
    return co(function *() {
      let script = yield this.sdk.Script.get(options.entity);
      script.data = data || Object.assign({}, this.sdk.request.query, this.sdk.request.body);
      let token = yield script.run();

      if (this.sdk.config.auth.oauth) token = yield this.checkLogin(token, name, options);
      return token;
    }.bind(this));
  }

  static checkLogin(token, name, data, options) {
    return co(function *() {
      if (token.user && token.access_token) return token;

      let id = undefined;
      if (token.id && token.id.toString().match(/^[0-9a-fA-F]{24}$/)) {
        id = token.id;
      }

      let user = yield this.sdk.User.find({
        $and: [{
          $or: [
            { id: id },
            { username: token.id },
            { username: token.username },
            { username: this.sdk.request.body.username }
          ]
        }, {
          $or: [
            { type: name },
            { type: { $exists: false } }
          ]
        }]
      }).catch(e => (console.error(e), null));

      console.log(token);
      data.username = data.username || token.username || token.id;
      data.password = data.password || token.password || Date.now();
      console.log(data);

      let userData = {
        username: data.username && data.username.toString().trim(),
        password: data.password && data.password.toString(),
        type: name,
        role: options.role || this.sdk.config.auth.defaultRole
      };

      user = user || new this.sdk.User({});
      yield user.update(userData);
      return this.integratedLogin('oauth', data, this.sdk.config.auth.oauth);
    }.bind(this));
  }

  static logout(data, method) {
    return co(function *() {
      let options = this.sdk.config.auth || {};
      let result;

      if (method) {
        if (options[method].entity)
          result = yield this.customLogin(method, data, options[method]);
        else
          result = yield this.integratedLogin(method, data, options[method]);
        this.sdk.isAuthorised = false;
        return result;
      }

      let errors = [];
      let keysLogout = Object.keys(options);

      if (options.oauth) {
        keysLogout.splice(keysLogout.indexOf('oauth'), 1);
        keysLogout.unshift('oauth');
      }

      for (let i = 0; i < keysLogout.length; i++) {
        let opts = options[keysLogout[i]];
        if (!opts) continue;

        if (opts.entity) {
          result = yield this.customLogout(keysLogout[i], data, opts)
            .catch(e => (errors.push(e), null));
        } else {
          result = yield this.integratedLogout(keysLogout[i], data, opts)
            .catch(e => (errors.push(e), null));
        }

        if (result) break;
      }

      this.sdk.isAuthorised = false;
      return result;
    }.bind(this));
  }

  static integratedLogout(name, data, options) {
    return co(function *() {
      name = name.replace(/[\/\\]/, '').toLowerCase();
      let IntegratedLogout = require(`./AuthMethods/${name}`);
      let integratedLogout = new IntegratedLogout(this.sdk, data, options);
      return integratedLogout.logout && integratedLogout.logout();
    }.bind(this));
  }

  static customLogout(name, data, options) {
    return co(function *() {
      let script = yield this.sdk.Script.get(options.entity);
      script.data = data || Object.assign({}, this.sdk.request.query, this.sdk.request.body);
      return script.run();
    }.bind(this));
  }

  static refresh(data, method) {
    return co(function *() {
      let options = this.sdk.config.auth || {};

      if (method) {
        if (options[method].entity)
          return this.customLogin(method, data, options[method]);
        else
          return this.integratedLogin(method, data, options[method]);
      }

      let errors = [];
      let keysRefresh = Object.keys(options);

      if (options.oauth) {
        keysRefresh.splice(keysRefresh.indexOf('oauth'), 1);
        keysRefresh.unshift('oauth');
      }

      for (let i = 0; i < keysRefresh.length; i++) {
        let opts = options[keysRefresh[i]];
        if (!opts) continue;

        if (opts.entity) {
          this.sdk.accessToken = yield this.customRefresh(keysRefresh[i], data, opts)
            .catch(e => (errors.push(e), null));
        } else {
          this.sdk.accessToken = yield this.integratedRefresh(keysRefresh[i], data, opts)
            .catch(e => (errors.push(e), null));
        }

        if (this.sdk.accessToken) break;
      }

      if (!this.sdk.accessToken) {
        throw new this.sdk.Error(400, errors);
      }

      return this.sdk.accessToken;
    }.bind(this));
  }

  static integratedRefresh(name, data, options) {
    return co(function *() {
      name = name.replace(/[\/\\]/, '').toLowerCase();
      let IntegratedRefresh = require(`./AuthMethods/${name}`);
      let integratedRefresh = new IntegratedRefresh(this.sdk, data, options);
      return integratedRefresh.refresh && integratedRefresh.refresh();
    }.bind(this));
  }

  static customRefresh(name, data, options) {
    return co(function *() {
      let script = yield this.sdk.Script.get(options.entity);
      script.data = data || Object.assign({}, this.sdk.request.query, this.sdk.request.body);
      return script.run();
    }.bind(this));
  }
}

module.exports = Common.SdkClass(Auth);
