'use strict';

const co = require('co');

const { SdkClass, SdkData, SdkArrayData } = require('./Common');

const symbols = {};
const sym = name => (symbols[name] || (symbols[name] = Symbol(name)));
const required = true;

class Role extends SdkData {
  constructor(data = {}) {
    super();
    this[sym('data')] = {};
    Object.assign(this[sym('data')], data);
  }

  schema() { return this.constructor.schema; }
  toObject() { return super.toObject(this[sym('data')]); }

  static create(data = {}) {
    let role = new this(data);
    return role.save();
  }

  get tablename() { return this.sdk.appId ? `Role_${this.sdk.appId}` : 'Role'; }
  static get tablename() { return this.sdk.appId ? `Role_${this.sdk.appId}` : 'Role'; }

  static get schema() {
    return {
      name: { type: 'string', required },
      permissions: { type: 'object', default: {} },
    };
  }

  static get schemaOptions() {
    return {
      timestamps: true,
      index: [
        [['name'], { unique: true }],
      ]
    };
  }

  static findAll(params = {}, options = {}) {
    return co(function *() {
      let roles = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions).findAll(params, options);
      roles = roles.map(r => new this(Object.assign({}, r)));
      let RolesClass = SdkClass(Roles)(this.sdk);
      return new RolesClass(roles);
    }.bind(this));
  }

  static find(params = {}, options = {}) {
    return co(function *() {
      let role = yield this.sdk.Adapter.init(this.tablename, this.schema, this.schemaOptions).find(params, options);
      if (!role) throw new this.sdk.Error(404, 'Role not found');
      return new this(role);
    }.bind(this));
  }

  static validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'create'].indexOf(access) > -1;

      case 'own':
        if (['list', 'view', 'update'].indexOf(access) > -1)
          return this.sdk.role ? { filter: { id: this.sdk.role.id } } : {};
        return false;

      default: return false;
    }
  }

  validatePermission(name, access) {
    switch (name) {
      case 'all':
        return ['list', 'view', 'create', 'update', 'delete'].indexOf(access) > -1;

      case 'own':
        if (this.id == this.sdk.role.id)
          if (['list', 'view', 'update'].indexOf(access) > -1)
            return this.sdk.role ? { filter: { id: this.sdk.role.id } } : {};
        return false;

      default: return false;
    }
  }

  get id() { return this[sym('data')].id ? this[sym('data')].id.toString() : null; }
  get name() { return this[sym('data')].name; }
  get permissions() { return this[sym('data')].permissions; }

  set name(value) { this[sym('data')].name = value; }
  set permissions(value) { this[sym('data')].permissions = value; }

  save() {
    return co(function *() {
      let role;
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;

      if (this.id) {
        role = yield this.sdk.Adapter.init(this.tablename, schema, options).update(this[sym('data')]);
      } else {
        role = yield this.sdk.Adapter.init(this.tablename, schema, options).insert(this[sym('data')]);
      }

      Object.keys(role).map(k => this[sym('data')][k] = role[k]);

      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      Object.keys(newData).map(f => this[f] = newData[f]);
      return this.save();
    }.bind(this));
  }

  remove() {
    return co(function *() {
      let schema = this.constructor.schema;
      let options = this.constructor.schemaOptions;
      yield this.sdk.Adapter.init(this.tablename, schema, options).remove({ id: this.id });
      Object.keys(this[sym('data')]).map(k => delete this[sym('data')][k]);
      return this;
    }.bind(this));
  }
}

class Roles extends SdkArrayData {
  schema() { return Role.schema; }

  save() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].save();
      return this;
    }.bind(this));
  }

  update(newData) {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].update(newData);
      return this;
    }.bind(this));
  }

  remove() {
    return co(function *() {
      for (let i = 0; i < this.length; i++) yield this[i].remove(newData);
      this.map((d, i) => delete this[i]);
      this.length = 0;
      return this;
    }.bind(this));
  }
}

module.exports = SdkClass(Role);
