'use strict';

const co = require('co');

const applications = {};
const entities = {};

class SDK {
  constructor(appId) {
    this.appId = appId || undefined;
    this.entity = {};
    this.table = {};
    this.service = {};
    this.script = {};
    this.request = {};

    this.config = Object.assign({}, SDK.Config());

    this.Application = SDK.Application(this);
    this.Adapter = SDK.Adapter(this);
    this.Auth = SDK.Auth(this);
    this.User = SDK.User(this);
    this.Device = SDK.Device(this);
    this.Role = SDK.Role(this);
    this.Entity = SDK.Entity(this);
    this.Table = SDK.Table(this);
    this.Service = SDK.Service(this);
    this.Script = SDK.Script(this);
    this.Swagger = SDK.Swagger(this);
    this.Error = SDK.Error;
  }

  middlewareExpress(appKey) { return SDK.middlewareExpress(appKey); }
  static middlewareExpress(appKey) {
    return function(req, res, next) {
      co(function *() {
        req.sdk = new SDK(req.headers[appKey]);
        yield req.sdk.init({
          request: {
            req: req.req,
            method: req.method,
            body: req.body || {},
            query: req.query || {},
            params: req.params || {},
            headers: req.headers
          }
        }).catch(e => null);
        yield req.sdk.Auth.authorize().catch(e => null);
      }.bind(this)).then(() => next()).catch(e => next(e));
    };
  }

  middlewareExpressError() { return SDK.middlewareExpressError(); }
  static middlewareExpressError() {
    return function(e, req, res, next) {
      console.error(e ? e.stack || e : '');
      let { status = 500, body = 'Internal error' } = SDK.getError(e) || {};
      res.status(status).send(body);
    };
  }

  middlewareKoa(appKey) { return SDK.middlewareKoa(appKey); }
  static middlewareKoa(appKey) {
    return function *(next) {
      try {
        this.sdk = new SDK(this.headers[appKey]);
        yield this.sdk.init({
          request: {
            req: this.req,
            method: this.method,
            body: this.request.fields || {},
            query: this.query || {},
            params: this.params || {},
            headers: this.headers
          }
        }).catch(e => null);
        yield this.sdk.Auth.authorize().catch(e => null);

        yield next;
      } catch (e) {
        if (!this.sdk || this.sdk.config.debug) console.error(e ? e.stack || e : '');

        if (this.sdk) {
          e = this.sdk.getError(e);
        } else {
          e = SDK.getError(e);
        }

        let { status = 500, body = 'Internal error' } = e || {};
        this.status = status;
        this.body = body;
        this.type = typeof body == 'object' ? 'application/json' : 'text/plain; charset=utf-8';
      }
    };
  }

  getError(errs) { return SDK.getError.call(this, errs); }
  static getError(errs) {
    if (!errs) return;
    errs = Array.isArray(errs) ? errs : [errs];

    let errors = [];
    for (let i = 0; i < errs.length; i++) {
      let err = errs[i];
      let error = {};
      if (err instanceof SDK.Error) {
        error.status = err.status;
        error.body = {
          error: {
            status: err.status,
            innerStatus: err.innerStatus,
            message: err.message,
          }
        };
        if (this.config && this.config.debug) error.body.error.stack = err.stack.split('\n');

        let pErr = this.getError(err.error);
        if (pErr) {
          if (Array.isArray(pErr)) error.body.error.parentErrors = pErr.map(e => e && e.body);
          if (!Array.isArray(pErr)) error.body.error.parentError = pErr.body;
        }

        if (!pErr && !error.body.error.message) error = undefined;
      } else if (typeof err == 'string') {
        error.body = err;
        if (!error.body) error = undefined;
      } else {
        error.body = {
          error: {
            status: err.status || err.statusCode || err.code,
            message: err.message
          }
        };
        if (!error.body.error.message) error = undefined;
        if (error && this.config && this.config.debug) error.body.error.stack = err.stack.split('\n');
      }
      errors.push(error);
    }

    return errors.length > 1 ? errors : errors[0];
  }

  init({ request = {} } = {}) {
    return co(function *() {
      this.request = request;
      let app = applications[this.appId];
      if (app) app = new this.Application(app);
      if (!app) app = yield this.Application.find({ id: this.appId }).catch(e => null);
      this.config = Object.assign(
        { auth: {}, register: {}, rootDir: __dirname + '/../..' },
        this.config, app && app.options || {}
      );
      if (this.config.auth.oauth === undefined) this.config.auth.oauth = SDK.Config().auth.oauth;
      if (request.headers && request.headers.debug) this.config.debug = true;

      if (!app) return this;

      this.application = app;
      applications[this.appId] = app.toObject();
      if (this.config.cacheTime) applications[this.appId]._cacheExpire = Date.now() + this.config.cacheTime;

      if (this.config.fetchAll) {
        let ents = yield this.Entity.findAll({});
        ents.filter(e => this[e.type] && !entities[e.id]).map(e => {
          entities[e.id] = new this[e.type](e);
          this.entity[e.id] = entities[e.id];
          this[e.type.toLowerCase()][e.id] = entities[e.id];
          this[e.type.toLowerCase()][e.name] = entities[e.id];

          if (this.config.cacheTime) entities[e.id]._cacheExpire = Date.now() + this.config.cacheTime;
        });
      }

      return this;
    }.bind(this));
  };

  dropApplicationCache(appId) {
    delete applications[appId];
  }

  dropEntityCache(entId) {
    delete entities[entId];
  }

  dropAllEntityCache() {
    for (let entId in entities) {
      this.dropEntityCache(entId);
    }
  }

  static configure(options) { return this.Config.configure(options, true); }

  static get Common() { return require('./models/Common'); }
  static get Config() { return require('./models/Config'); }
  static get Application() { return require('./models/Application'); }
  static get Error() { return require('./models/Error'); }
  static get Adapter() { return require('./models/Adapter'); }
  static get Auth() { return require('./models/Auth'); }
  static get User() { return require('./models/User'); }
  static get Device() { return require('./models/Device'); }
  static get Role() { return require('./models/Role'); }
  static get Entity() { return require('./models/Entity'); }
  static get Table() { return require('./models/Table'); }
  static get Service() { return require('./models/Service'); }
  static get Script() { return require('./models/Script'); }
  static get KoaRouter() { return require('./models/KoaRouter'); }
  static get ExpressRouter() { return require('./models/ExpressRouter'); }
  static get Swagger() { return require('./models/Swagger'); }
}

module.exports = SDK;

(function clearCache() {
  for (let appId in applications) {
    //if (applications[appId]._cacheExpire && applications[appId]._cacheExpire <= Date.now()) {
    //  delete applications[appId];
    //}
  }

  for (let entId in entities) {
    if (entities[entId]._cacheExpire && entities[entId]._cacheExpire <= Date.now()) {
      delete entities[entId];
    }
  }

  if (!SDK.noCache) {
    setTimeout(clearCache, 1000);
  }
})();
